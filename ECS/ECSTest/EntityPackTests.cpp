// Precompiled Header
#include "pch.h"
// Project Headers
#include "SharedTypes.h"
#include "../ECS/SparseSet.h"
#include "../ECS/Utility.h"
#include "../ECS/TypeId.h"
#include "../ECS/ECS.h"
TEST(EntityPack, ConstructSingleArg)
{
    Pls::EntityPack<int> pack(Pls::Entity(0), 0);
}
TEST(EntityPack, ConstructSingleArgClass)
{
    Int i(10);
    Pls::EntityPack<Int> pack(Pls::Entity(0), &i);
}
TEST(EntityPack, ConstructMultiArg)
{
    int x = 0;
    double d = 4.2;
    Int i(9);
    Pls::EntityPack<int, double, Int> pack(Pls::Entity(0), &x, &d, & i);
}
TEST(EntityPack, GetSingleArg)
{
    int i = 6;
    Pls::EntityPack<int> pack(Pls::Entity(0), &i);
    EXPECT_EQ(pack.Get<int>(), 6);
}
TEST(EntityPack, GetSingleArgClass)
{
    Int i(6);
    Pls::EntityPack<Int> pack(Pls::Entity(0), &i);
    EXPECT_EQ(pack.Get<Int>().i, 6);
}
TEST(EntityPack, GetMultiArg)
{
    int x = 0;
    double d = 4.2;
    Int i(9);
    Pls::EntityPack<int, double, Int> pack(Pls::Entity(0), &x, &d, & i);
    EXPECT_EQ(pack.Get<int>(), 0);
    EXPECT_EQ(pack.Get<double>(), 4.2);
    EXPECT_EQ(pack.Get<Int>().i, 9);
}