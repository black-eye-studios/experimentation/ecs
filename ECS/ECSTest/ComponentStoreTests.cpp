// Precompiled Header
#include "pch.h"
// Project Headers
#include "SharedTypes.h"
#include "../ECS/Entity.h"
#include "../ECS/ComponentStore.h"

TEST(ComponentStore, AddComponent)
{
    Pls::Entity entity = Pls::Entity(0);
    Pls::ComponentStore store;
    Int& i = store.Add<Int>(entity);
    i.i = 10;

    EXPECT_EQ(i.i, 10);
}
TEST(ComponentStore, GetComponent)
{
    Pls::Entity entity = Pls::Entity(0);
    Pls::ComponentStore store;
    EXPECT_TRUE(store.Get<Int>(entity) == nullptr);
    store.Add<Int>(entity);
    
    EXPECT_TRUE(store.Get<Int>(entity) != nullptr);
    EXPECT_TRUE(store.Get<int>(entity) == nullptr);
}
TEST(ComponentStore, GetComponentValueRetained)
{
    Pls::Entity entity = Pls::Entity(0);
    Pls::ComponentStore store;
    Int& component = store.Add<Int>(entity);
    EXPECT_EQ(component.i, Int().i);
    component.i = 5;
    Int* retrieved = store.Get<Int>(entity);
    EXPECT_TRUE(retrieved != nullptr);
    EXPECT_EQ(retrieved->i, Int().i);
}
TEST(ComponentStore, RemoveComponent)
{
    Pls::Entity entity = Pls::Entity(0);
    Pls::ComponentStore store;
    store.Add<Int>(entity);
    EXPECT_TRUE(store.Get<Int>(entity) != nullptr);
    store.Remove<Int>(entity);
    EXPECT_TRUE(store.Get<Int>(entity) == nullptr);
}
TEST(ComponentStore, GetCount)
{
    Pls::ComponentStore store;
    store.Add<Int>(Pls::Entity(0));
    store.Add<Int>(Pls::Entity(11));
    store.Add<Int>(Pls::Entity(219));
    store.Add<Int>(Pls::Entity(32));

    EXPECT_EQ(store.GetCount<Int>(), 4);
}

TEST(ComponentStore, DenseInverseIterator)
{
    Pls::ComponentStore store;
    store.Add<Int>(Pls::Entity(0));
    store.Add<Int>(Pls::Entity(11));
    store.Add<Int>(Pls::Entity(219));
    store.Add<Int>(Pls::Entity(32));
    
    // Size Check
    auto range = store.GetEntities<Int>();
    ASSERT_EQ(range.size(), 4);

    // Values Check
    const auto& begin = range.begin();
    EXPECT_EQ(*(begin + 0), 0);
    EXPECT_EQ(*(begin + 1), 11);
    EXPECT_EQ(*(begin + 2), 219);
    EXPECT_EQ(*(begin + 3), 32);
}

TEST(ComponentStore, DenseInverseRangeBasedFor)
{
    constexpr Pls::Entity::IDType IDS[] = { 0, 11, 219, 32 };

    Pls::ComponentStore store;
    store.Add<Int>(Pls::Entity(IDS[0]));
    store.Add<Int>(Pls::Entity(IDS[1]));
    store.Add<Int>(Pls::Entity(IDS[2]));
    store.Add<Int>(Pls::Entity(IDS[3]));

    // Size Check
    auto range = store.GetEntities<Int>();
    ASSERT_EQ(range.size(), sizeof(IDS) / sizeof(Pls::Entity::IDType));

    // Range Check
    int counter = 0;
    for (auto& entity : range)
    {
        EXPECT_EQ(entity, IDS[counter++]);
    }
}