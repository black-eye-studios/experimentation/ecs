// Precompiled Header
#include "pch.h"
// Project Headers
#include "SharedTypes.h"
#include "../ECS/TypeId.h"
#include "../ECS/SparseSet.h"

TEST(TypeIdGenerator, Primitives)
{
    
    Pls::TypeId::Get<int>();
    Pls::TypeId::Get<float>(); 
    Pls::TypeId::Get<double>(); 
}

TEST(TypeIdGenerator, Classes)
{
    Pls::TypeId::Get<std::string>(); 
    Pls::TypeId::Get<Pls::TypeId>(); 
}

TEST(TypeIdGenerator, Combination)
{
    Pls::TypeId::Get<std::string, int>(); 
    Pls::TypeId::Get<Pls::TypeId, float>(); 
}

TEST(TypeIdGenerator, TemplateClasses)
{
    Pls::TypeId::Get<Pls::SparseSet<int>>(); 
    Pls::TypeId::Get<Pls::SparseSet<int>>(); 
}

TEST(TypeIdGenerator, PrimitivesReproducible)
{
    
    EXPECT_EQ(Pls::TypeId::Get<int>().GetId(), Pls::TypeId::Get<int>().GetId());
    EXPECT_EQ(Pls::TypeId::Get<float>().GetId(), Pls::TypeId::Get<float>().GetId()); 
    EXPECT_EQ(Pls::TypeId::Get<double>().GetId(), Pls::TypeId::Get<double>().GetId()); 
}

TEST(TypeIdGenerator, ClassesReproducible)
{
    EXPECT_EQ(Pls::TypeId::Get<std::string>().GetId(), Pls::TypeId::Get<std::string>().GetId());
    EXPECT_EQ(Pls::TypeId::Get<Pls::TypeId>().GetId(), Pls::TypeId::Get<Pls::TypeId>().GetId());
}

TEST(TypeIdGenerator, TemplateClassesReproducible)
{
    EXPECT_EQ((Pls::TypeId::Get<Pls::SparseSet<int>>().GetId()), (Pls::TypeId::Get<Pls::SparseSet<int>>().GetId()));
    EXPECT_EQ((Pls::TypeId::Get<Pls::SparseSet<int>>().GetId()), (Pls::TypeId::Get<Pls::SparseSet<int>>().GetId()));
    EXPECT_EQ((Pls::TypeId::Get<std::vector<int>>().GetId()), (Pls::TypeId::Get<std::vector<int>>().GetId()));
    EXPECT_EQ((Pls::TypeId::Get<std::vector<std::string>>().GetId()), (Pls::TypeId::Get<std::vector<std::string>>().GetId()));
}

TEST(TypeIdGenerator, Equality)
{
    EXPECT_TRUE(Pls::TypeId::Get<std::string>() == Pls::TypeId::Get<std::string>());
    EXPECT_TRUE(Pls::TypeId::Get<Pls::TypeId>() == Pls::TypeId::Get<Pls::TypeId>());
    EXPECT_FALSE(Pls::TypeId::Get<int>() == Pls::TypeId::Get<double>());
    EXPECT_FALSE(Pls::TypeId::Get<double>() == Pls::TypeId::Get<char>());
}

TEST(TypeIdGenerator, Inequality)
{
    EXPECT_TRUE(Pls::TypeId::Get<std::string>() != Pls::TypeId::Get<Pls::TypeId>());
    EXPECT_TRUE(Pls::TypeId::Get<Pls::TypeId>() != Pls::TypeId::Get<std::string>());
    EXPECT_FALSE(Pls::TypeId::Get<short>() != Pls::TypeId::Get<short>());
    EXPECT_FALSE(Pls::TypeId::Get<size_t>() != Pls::TypeId::Get<size_t>());
}