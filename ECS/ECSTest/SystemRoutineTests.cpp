// Precompiled Header
#include "pch.h"
// Project Headers
#include "SharedTypes.h"
#include "../ECS/SparseSet.h"
#include "../ECS/Utility.h"
#include "../ECS/TypeId.h"
#include "../ECS/ECS.h"


TEST(SystemRoutine, CreateStandalone)
{
    class TestRoutine : public Pls::SystemRoutine<void, int>
    {
      public:
        using Pls::SystemRoutine<void, int>::SystemRoutine;
        void execute(Pls::SceneView<int>&) override
        {
        }

    };
    TestRoutine routine(nullptr);

    Pls::ComponentStore store;
    Pls::EntityPackList<int> epl(store);
    Pls::SceneView<int> sv(epl);
    routine.execute(sv);
}
TEST(SystemRoutine, CreatePartOfSystem)
{
    class SomeSystem : public Pls::System<SomeSystem>
    {
      public:
        SomeSystem()
        : routine{ this }
        {}


        class TestRoutine : public Routine<int>
        {
            using Routine<int>::Routine;
          public:
            void execute(Pls::SceneView<int>&) override
            {
            }

        };
        TestRoutine routine;
    };
    


    SomeSystem parent;    
    Pls::ComponentStore store;
    Pls::EntityPackList<int> epl(store);
    Pls::SceneView<int> sv(epl);
    parent.routine.execute(sv);
}

// TODO: Add support in future
//TEST(SystemRoutine, CreateNoDependency)
//{
//    class TestRoutine : public Pls::SystemRoutine<void>
//    {
//      public:
//        using Pls::SystemRoutine<void>::SystemRoutine;
//        void execute(const Pls::SceneView<>&) override
//        {
//        }
//
//    };
//    TestRoutine routine(nullptr);
//    
//    Pls::ComponentStore store;
//    Pls::SceneView sv(store);
//    routine.execute(sv);
//}

TEST(SystemRoutine, CreateMultipleDependency)
{
    class TestRoutine : public Pls::SystemRoutine<void, int, float, bool, double>
    {
      public:
        using Pls::SystemRoutine<void, int, float, bool, double>::SystemRoutine;
        void execute(Pls::SceneView<int, float, bool, double>&)
        {
        }

    };
    TestRoutine routine(nullptr);
    
    Pls::ComponentStore store;
    Pls::EntityPackList<int, float, bool, double> epl(store);
    Pls::SceneView<int, float, bool, double> sv(epl);
    routine.execute(sv);
}