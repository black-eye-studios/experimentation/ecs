// Precompiled Header
#include "pch.h"
// Project Headers
#include "SharedTypes.h"
#include "../ECS/Utility.h"

TEST(Hash, Crc32Unique)
{
    EXPECT_EQ(Pls::HashCrc32("hello world"), 0x0d4a1185);
    EXPECT_EQ(Pls::HashCrc32("uhisdch@#@!uifhui!329S!@$"), 0xa41ee440);
    EXPECT_EQ(Pls::HashCrc32("brown dog jumps over the lazy dog"), 0xeed0e937);
    EXPECT_EQ(Pls::HashCrc32("Pls stands for plushie"), 0xf47edd17);
    EXPECT_EQ(Pls::HashCrc32("Pls stands for plushid"), 0x8379ed81);
    EXPECT_EQ(Pls::HashCrc32("pLs sTAnDs F0r p1U5hi3"), 0x9cf4ce4c);
}

TEST(Hash, Crc32Reproducible)
{
    EXPECT_EQ(Pls::HashCrc32("hello world"), Pls::HashCrc32("hello world"));
    EXPECT_EQ(Pls::HashCrc32("uhisdch@#@!uifhui!329S!@$"), Pls::HashCrc32("uhisdch@#@!uifhui!329S!@$"));
    EXPECT_EQ(Pls::HashCrc32("brown dog jumps over the lazy dog"), Pls::HashCrc32("brown dog jumps over the lazy dog"));
    EXPECT_EQ(Pls::HashCrc32("Pls stands for plushie"), Pls::HashCrc32("Pls stands for plushie"));
    EXPECT_EQ(Pls::HashCrc32("Pls stands for plushid"), Pls::HashCrc32("Pls stands for plushid"));
    EXPECT_EQ(Pls::HashCrc32("pLs sTAnDs F0r p1U5hi3"), Pls::HashCrc32("pLs sTAnDs F0r p1U5hi3"));
}