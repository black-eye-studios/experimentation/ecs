// Precompiled Header
#include "pch.h"
// Project Headers
#include "SharedTypes.h"
#include "../ECS/SparseSet.h"
#include "../ECS/Utility.h"
#include "../ECS/TypeId.h"
#include "../ECS/ECS.h"

TEST(SceneView, ConstructEmptyView)
{
    Pls::ComponentStore store;
    Pls::EntityPackList<int> epl(store);
    Pls::SceneView<int> sceneView(epl);
}
TEST(SceneView, ConstructFilledView)
{
    Pls::ComponentStore store;
    store.Add<int>(Pls::Entity(0));
    store.Add<int>(Pls::Entity(1));
    store.Add<int>(Pls::Entity(2));
    Pls::EntityPackList<int> epl(store);
    Pls::SceneView<int> sceneView(epl);
}
TEST(SceneView, ConstructMultiType)
{    
    Pls::ComponentStore store;
    store.Add<int>(Pls::Entity(0));
    store.Add<int>(Pls::Entity(1));
    store.Add<int>(Pls::Entity(2));
    store.Add<bool>(Pls::Entity(0));
    store.Add<bool>(Pls::Entity(1));
    store.Add<bool>(Pls::Entity(2));
    store.Add<float>(Pls::Entity(0));
    store.Add<float>(Pls::Entity(1));
    store.Add<float>(Pls::Entity(2));
    Pls::EntityPackList<int, bool, float> epl(store);
    Pls::SceneView<int, bool, float> sceneView(epl);
}
TEST(SceneView, Iterator)
{
    Pls::ComponentStore store;
    store.Add<int>(Pls::Entity(0)) = 0;
    store.Add<int>(Pls::Entity(1)) = 1;
    store.Add<int>(Pls::Entity(2)) = 2;
    Pls::EntityPackList<int> epl(store);
    Pls::SceneView<int> sceneView(epl);
    
    EXPECT_EQ((sceneView.begin()    )->Get<int>(), 0);
    EXPECT_EQ((sceneView.begin() + 1)->Get<int>(), 1);
    EXPECT_EQ((sceneView.begin() + 2)->Get<int>(), 2);
}
TEST(SceneView, ConstIterator)
{
    Pls::ComponentStore store;
    store.Add<int>(Pls::Entity(0)) = 0;
    store.Add<int>(Pls::Entity(1)) = 1;
    store.Add<int>(Pls::Entity(2)) = 2;
    Pls::EntityPackList<int> epl(store);
    Pls::SceneView<int> sceneView(epl);
    
    EXPECT_EQ((sceneView.cbegin()    )->Get<int>(), 0);
    EXPECT_EQ((sceneView.cbegin() + 1)->Get<int>(), 1);
    EXPECT_EQ((sceneView.cbegin() + 2)->Get<int>(), 2);
}
TEST(SceneView, RangeBasedForLoop)
{
    Pls::ComponentStore store;
    store.Add<int>(Pls::Entity(0)) = 0;
    store.Add<int>(Pls::Entity(1)) = 1;
    store.Add<int>(Pls::Entity(2)) = 2;
    Pls::EntityPackList<int> epl(store);
    Pls::SceneView<int> sceneView(epl);
    
    int value = 0;
    for (auto& pack : sceneView)
    {
        EXPECT_EQ(pack.Get<int>(), value++);
    }
}