// Precompiled Header
#include "pch.h"
// Project Headers
#include "../ECS/SparseSet.h"
#include "../ECS/Utility.h"
#include "../ECS/TypeId.h"
#include "../ECS/ECS.h"


TEST(Entity, Create)
{
    // Construction test, should not throw errors
    Pls::Entity entity = Pls::Entity(1);
    entity = Pls::Entity(1, 2);
}

TEST(Entity, RetrieveID)
{
    Pls::Entity entity = Pls::Entity(1, 2);
    EXPECT_EQ(entity.GetID(), 1);
}

TEST(Entity, RetrieveVersion)
{
    Pls::Entity entity = Pls::Entity(1, 2);
    EXPECT_EQ(entity.GetVersion(), 2);
}

TEST(Entity, Equals)
{
    EXPECT_TRUE(Pls::Entity(1, 2) == Pls::Entity(1, 2));
    EXPECT_FALSE(Pls::Entity(1, 0) == Pls::Entity(1, 1));
    EXPECT_FALSE(Pls::Entity(12, 0) == Pls::Entity(41, 1));
}

TEST(Entity, NotEquals)
{
    EXPECT_FALSE(Pls::Entity(1, 2) != Pls::Entity(1, 2));
    EXPECT_TRUE(Pls::Entity(1, 0) != Pls::Entity(1, 1));
    EXPECT_TRUE(Pls::Entity(12, 0) != Pls::Entity(41, 1));
}