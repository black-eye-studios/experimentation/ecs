// Precompiled Header
#include "pch.h"
// Standard Library
#include <unordered_map>
// Project Headers
#include "SharedTypes.h"
#include "../ECS/SparseSet.h"

TEST(SparseSet, ConstructWithBasicType) 
{
    Pls::SparseSet<int> set;
    EXPECT_TRUE(true);
}

TEST(SparseSet, ConstructWithClassType) 
{
    Pls::SparseSet<Int> set;
    EXPECT_TRUE(true);
}

TEST(SparseSet, Subscript)
{
    Pls::SparseSet<Int> set;
    set.insert(1).i = 5;
    set.insert(8).i = 32;
    set.insert(10).i = 675;

    EXPECT_EQ(set[1].i, 5);
    EXPECT_EQ(set[8].i, 32);
    EXPECT_EQ(set[10].i, 675);
}

TEST(SparseSet, ConstSubscript)
{
    Pls::SparseSet<Int> set;
    set.insert(1).i = 5;
    set.insert(8).i = 32;
    set.insert(10).i = 675;

    const Pls::SparseSet<Int>& constSet = set;
    EXPECT_EQ(constSet[1].i, 5);
    EXPECT_EQ(constSet[8].i, 32);
    EXPECT_EQ(constSet[10].i, 675);
}

TEST(SparseSet, FillWithBasicType) 
{
    Pls::SparseSet<int> set;
    set.insert(1) = 5;
    set.insert(8) = 32;
    set.insert(10) = 675;
    set.insert(5) = 4823;
    
    EXPECT_EQ(set[1], 5);
    EXPECT_EQ(set[8], 32);
    EXPECT_EQ(set[10], 675);
    EXPECT_EQ(set[5], 4823);
}

TEST(SparseSet, FillWithClassType) 
{
    Pls::SparseSet<Int> set;
    set.insert(1).i = 5;
    set.insert(8).i = 32;
    set.insert(10).i = 675;
    set.insert(5).i = 4823;
    
    EXPECT_EQ(set[1].i, 5);
    EXPECT_EQ(set[8].i, 32);
    EXPECT_EQ(set[10].i, 675);
    EXPECT_EQ(set[5].i, 4823);
}

TEST(SparseSet, Size) 
{
    Pls::SparseSet<int> set;
    set.insert(1) = 5;
    set.insert(8) = 32;
    set.insert(10) = 675;
    set.insert(5) = 4823;
    EXPECT_EQ(set.size(), 4);
}

TEST(SparseSet, Erase) 
{
    Pls::SparseSet<int> set;
    set.insert(1) = 5;
    set.insert(8) = 32;
    set.insert(10) = 675;
    set.insert(5) = 4823;
    set.erase(1);
    EXPECT_EQ(set.size(), 3);
    set.erase(8);
    set.erase(10);
    EXPECT_EQ(set.size(), 1);
    set.insert(10) = 675;
    set.insert(2) = 4823;
    set.erase(2);
    EXPECT_EQ(set.size(), 2);
    set.erase(10);
    set.erase(5);
    EXPECT_EQ(set.size(), 0);

}

TEST(SparseSet, Capacity) 
{
    Pls::SparseSet<int> set;
    EXPECT_EQ(set.capacity(), 256);
    set.insert(1) = 5;
    set.insert(8) = 32;
    set.insert(10) = 675;
    
    EXPECT_EQ(set.capacity(), 256);
}

TEST(SparseSet, Empty) 
{
    Pls::SparseSet<int> set;
    EXPECT_TRUE(set.empty());

    set.insert(1) = 5;
    set.insert(8) = 32;
    set.insert(10) = 675;
    set.insert(5) = 4823;
    EXPECT_FALSE(set.empty());
    
    set.erase(1);
    set.erase(8);
    set.erase(10);
    set.erase(5);
    EXPECT_TRUE(set.empty());
}

TEST(SparseSet, Clear) 
{
    Pls::SparseSet<int> set;
    set.insert(1) = 5;
    set.insert(8) = 32;
    set.insert(10) = 675;
    set.insert(5) = 4823;
    set.clear();
    
    EXPECT_TRUE(set.empty());
}

TEST(SparseSet, GenericClear) 
{
    Pls::SparseSet<int> set;
    set.insert(1) = 5;
    set.insert(8) = 32;
    set.insert(10) = 675;
    set.insert(5) = 4823;
    set.clear();

    Pls::SparseSetBase<>& setRef = set;
    setRef.clear();
    
    EXPECT_TRUE(set.empty());
}

TEST(SparseSet, Contains)
{
    Pls::SparseSet<int> set;
    set.insert(1) = 5;
    set.insert(8) = 32;
    set.insert(10) = 675;
    EXPECT_TRUE(set.contains(1));
    EXPECT_FALSE(set.contains(2));
    set.erase(8);
    EXPECT_TRUE(set.contains(1));
    EXPECT_FALSE(set.contains(8));
    set.insert(5) = 4823;
    set.erase(1);
    set.insert(8) = 38;
    set.erase(10);
    set.erase(5);
    set.insert(6) = 342;
    set.insert(82) = 23;
    set.erase(6);
    EXPECT_TRUE(set.contains(8));
    EXPECT_FALSE(set.contains(1));
    set.erase(8);    
    set.erase(82);
}

TEST(SparseSet, Find)
{
    Pls::SparseSet<int> set;
    set.insert(1) = 5;
    set.insert(8) = 32;
    set.insert(10) = 675;
    EXPECT_TRUE(set.find(1) != set.end());
    EXPECT_TRUE(set.find(2) == set.end());
    set.erase(8);
    EXPECT_TRUE(set.find(1) != set.end());
    EXPECT_TRUE(set.find(8) == set.end());
    set.insert(5) = 4823;
    set.erase(1);
    set.insert(8) = 38;
    set.erase(10);
    set.erase(5);
    set.insert(6) = 342;
    set.insert(82) = 23;
    set.erase(6);
    EXPECT_TRUE(set.find(8) != set.end());
    EXPECT_TRUE(set.find(1) == set.end());
    set.erase(8);    
    set.erase(82);
}

TEST(SparseSet, ConstFind)
{
    Pls::SparseSet<int> set;
    set.insert(1) = 5;
    set.insert(8) = 32;
    set.insert(10) = 675;
    set.erase(8);
    set.insert(5) = 4823;
    set.erase(1);
    set.insert(8) = 38;
    set.erase(10);
    set.erase(5);
    set.insert(6) = 342;
    set.insert(82) = 23;
    set.erase(6);

    const auto& constSet = set;

    EXPECT_TRUE(constSet.find(8) != constSet.cend());
    EXPECT_TRUE(constSet.find(1) == constSet.cend());
}

TEST(SparseSet, SequentialFillAndEraseWithBasicType) 
{
    Pls::SparseSet<int> set;
    set.insert(1) = 5;
    set.insert(8) = 32;
    set.insert(10) = 675;
    set.insert(5) = 4823;
    set.erase(1);
    set.erase(8);
    set.erase(10);
    set.erase(5);
    
    EXPECT_TRUE(set.empty());
}

TEST(SparseSet, SequentialFillAndEraseWithClassType) 
{
    Pls::SparseSet<Int> set;
    set.insert(1).i = 5;
    set.insert(8).i = 32;
    set.insert(10).i = 675;
    set.insert(5).i = 4823;
    set.erase(1);
    set.erase(8);
    set.erase(10);
    set.erase(5);
    
    EXPECT_TRUE(set.empty());
}

TEST(SparseSet, RandomFillAnderaseWithBasicType) 
{
    Pls::SparseSet<int> set;
    set.insert(1) = 5;
    set.insert(8) = 32;
    set.insert(10) = 675;
    set.erase(8);
    EXPECT_EQ(set.size(), 2);
    set.insert(5) = 4823;
    EXPECT_EQ(set.size(), 3);
    set.erase(1);
    set.insert(8) = 38;
    EXPECT_EQ(set.size(), 3);
    set.erase(10);
    set.erase(5);
    EXPECT_EQ(set.size(), 1);
    set.insert(6) = 342;
    set.insert(82) = 23;
    EXPECT_EQ(set.size(), 3);
    set.erase(6);
    set.erase(82);
    set.erase(8);    
    EXPECT_TRUE(set.empty());
}

TEST(SparseSet, RandomFillAnderaseWithClassType) 
{
    Pls::SparseSet<Int> set;
    set.insert(1).i = 5;
    set.insert(8).i = 32;
    set.insert(10).i = 675;
    set.erase(8);
    EXPECT_EQ(set.size(), 2);
    set.insert(5).i = 4823;
    EXPECT_EQ(set.size(), 3);
    set.erase(1);
    set.insert(8).i = 38;
    EXPECT_EQ(set.size(), 3);
    set.erase(10);
    set.erase(5);
    EXPECT_EQ(set.size(), 1);
    set.insert(6).i = 342;
    set.insert(82).i = 23;
    EXPECT_EQ(set.size(), 3);
    set.erase(6);
    set.erase(82);
    set.erase(8);    
    EXPECT_TRUE(set.empty());
}

TEST(SparseSet, IteratorWithBasicType)
{
    Pls::SparseSet<int> set;
    set.insert(1) = 1;
    set.insert(8) = 2;
    set.insert(10) = 3;
    set.insert(3) = 4;

    int total = 0;
    for (auto iter = set.begin(); iter != set.end(); ++iter)
    {
        total += *iter;
    }
    EXPECT_EQ(total, 10);
}

TEST(SparseSet, IteratorWithClassType)
{
    Pls::SparseSet<Int> set;
    set.insert(1).i = 1;
    set.insert(8).i = 2;
    set.insert(10).i = 3;
    set.insert(3).i = 4;

    int total = 0;
    for (auto iter = set.begin(); iter != set.end(); ++iter)
    {
        total += iter->i;
    }
    EXPECT_EQ(total, 10);
}

TEST(SparseSet, IteratorConstWithBasicType)
{
    Pls::SparseSet<int> set;
    set.insert(1) = 1;
    set.insert(8) = 2;
    set.insert(10) = 3;
    set.insert(3) = 4;

    int total = 0;
    for (auto iter = set.cbegin(); iter != set.cend(); ++iter)
    {
        total += *iter;
    }
    EXPECT_EQ(total, 10);
}

TEST(SparseSet, IteratorConstWithClassType)
{
    Pls::SparseSet<Int> set;
    set.insert(1).i = 1;
    set.insert(8).i = 2;
    set.insert(10).i = 3;
    set.insert(3).i = 4;

    int total = 0;
    for (auto iter = set.cbegin(); iter != set.cend(); ++iter)
    {
        total += iter->i;
    }
    EXPECT_EQ(total, 10);
}

TEST(SparseSet, IteratorReverseWithBasicType)
{
    Pls::SparseSet<int> set;
    set.insert(1) = 1;
    set.insert(8) = 2;
    set.insert(10) = 3;
    set.insert(3) = 4;

    int total = 0;
    for (auto iter = set.rbegin(); iter != set.rend(); ++iter)
    {
        total += *iter;
    }
    EXPECT_EQ(total, 10);
}

TEST(SparseSet, IteratorReverseWithClassType)
{
    Pls::SparseSet<Int> set;
    set.insert(1).i = 1;
    set.insert(8).i = 2;
    set.insert(10).i = 3;
    set.insert(3).i = 4;

    int total = 0;
    for (auto iter = set.rbegin(); iter != set.rend(); ++iter)
    {
        total += iter->i;
    }
    EXPECT_EQ(total, 10);
}

TEST(SparseSet, IteratorConstReverseWithBasicType)
{
    Pls::SparseSet<int> set;
    set.insert(1) = 1;
    set.insert(8) = 2;
    set.insert(10) = 3;
    set.insert(3) = 4;

    int total = 0;
    for (auto iter = set.crbegin(); iter != set.crend(); ++iter)
    {
        total += *iter;
    }
    EXPECT_EQ(total, 10);
}

TEST(SparseSet, IteratorConstReverseWithClassType)
{
    Pls::SparseSet<Int> set;
    set.insert(1).i = 1;
    set.insert(8).i = 2;
    set.insert(10).i = 3;
    set.insert(3).i = 4;

    int total = 0;
    for (auto iter = set.crbegin(); iter != set.crend(); ++iter)
    {
        total += iter->i;
    }
    EXPECT_EQ(total, 10);
}

TEST(SparseSet, DenseInverseIterator)
{
    Pls::SparseSet<int> set;
    set.insert(1) = 1;
    set.insert(8) = 2;
    set.insert(10) = 3;
    set.insert(3) = 4;
    
    // Size Check
    auto cbegin = set.dense_cbegin();
    ASSERT_EQ(set.dense_cend() - cbegin, 4);

    // Values Check
    EXPECT_EQ(cbegin[0], 1);
    EXPECT_EQ(cbegin[1], 8);
    EXPECT_EQ(cbegin[2], 10);
    EXPECT_EQ(cbegin[3], 3);
}

TEST(SparseSet, OrderOverrideFlip) 
{
    // Create set and populate it
    Pls::SparseSet<int> set;
    set.insert(1) = 1;
    set.insert(2) = 2;
    set.insert(3) = 3;
    set.insert(4) = 4;
    set.insert(5) = 5;
    set.insert(6) = 6;
    set.insert(7) = 7;
    set.insert(8) = 8;

    // Reorder
    const std::vector<decltype(set)::index_type> OVERRIDE_ORDER =
    {
        8, 7, 6, 5, 4, 3, 2, 1
    };
    set.order(OVERRIDE_ORDER);

    // Test the ordering
    decltype(set)::index_type counter = 0;
    std::for_each(set.cbegin(), set.cend(), [&](int i)
    {
        EXPECT_EQ(i, OVERRIDE_ORDER[counter++]);
    });

    // Test that sparse access is still the same
    EXPECT_EQ(set[1], 1);
    EXPECT_EQ(set[2], 2);
    EXPECT_EQ(set[3], 3);
    EXPECT_EQ(set[4], 4);
    EXPECT_EQ(set[5], 5);
    EXPECT_EQ(set[6], 6);
    EXPECT_EQ(set[7], 7);
    EXPECT_EQ(set[8], 8);
}

TEST(SparseSet, OrderOverrideRandom) 
{

    // Create set and populate it
    Pls::SparseSet<int> set;
    const std::unordered_map<decltype(set)::index_type, decltype(set)::value_type> TEST_DATA =
    {
        std::make_pair(21, 74354),
        std::make_pair(24, 322),
        std::make_pair(13, 224),
        std::make_pair(122, 856),
        std::make_pair(235, 1),
        std::make_pair(199, 8926),
        std::make_pair(7, 4525346),
        std::make_pair(162, 62377),

    };
    for (const auto& pair : TEST_DATA)
    {
        set.insert(pair.first) = pair.second;
    }

    // Reorder based on the following
    const std::vector<decltype(set)::index_type> OVERRIDE_ORDER =
    {
        235, 21, 13, 162, 7, 199, 24, 122
    };
    set.order(OVERRIDE_ORDER);

    // Test the ordering
    decltype(set)::index_type counter = 0;
    std::for_each(set.cbegin(), set.cend(), [&](int i)
    {
        EXPECT_EQ(i, TEST_DATA.at(OVERRIDE_ORDER[counter++]));
    });

    // Test that sparse access is still the same
    for (const auto& pair : TEST_DATA)
    {
        EXPECT_EQ(set[pair.first], pair.second);
    }
}