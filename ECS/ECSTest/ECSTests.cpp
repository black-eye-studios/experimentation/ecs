// Precompiled Header
#include "pch.h"
// Project Headers
#include "SharedTypes.h"
#include "../ECS/SparseSet.h"
#include "../ECS/Utility.h"
#include "../ECS/TypeId.h"
#include "../ECS/ECS.h"


class ECS : public ::testing::Test
{
  protected:
    void TearDown() override 
    {
        Pls::ECS::Reset();
    }
};
TEST_F(ECS, CreateEntity)
{
    Pls::Entity entity = Pls::ECS::CreateEntity();
    EXPECT_EQ(entity.GetID(), 0);
}
TEST_F(ECS, DestroyEntity)
{
    Pls::Entity entity = Pls::ECS::CreateEntity();
    Pls::ECS::DestroyEntity(entity);
}
TEST_F(ECS, AddComponent)
{
    Pls::Entity entity = Pls::ECS::CreateEntity();
    Pls::ECS::AddComponent<int>(entity);
}
TEST_F(ECS, AddComponentClass)
{
    Pls::Entity entity = Pls::ECS::CreateEntity();
    Pls::ECS::AddComponent<Int>(entity);
}
TEST_F(ECS, AddComponents)
{
    Pls::Entity entity = Pls::ECS::CreateEntity();
    Pls::EntityPack<int, Int, double> pack = Pls::ECS::AddComponents<int, Int, double>(entity);
    EXPECT_TRUE(pack.Get<int>() == int{});
    EXPECT_TRUE(pack.Get<Int>().i == Int().i);
    EXPECT_TRUE(pack.Get<double>() == double{});
}
TEST_F(ECS, CreateEntityWithComponents)
{
    Pls::EntityPack<int, Int, double> pack = Pls::ECS::CreateEntity<int, Int, double>();
    EXPECT_TRUE(pack.Get<int>() == int{});
    EXPECT_TRUE(pack.Get<Int>().i == Int().i);
    EXPECT_TRUE(pack.Get<double>() == double{});
}
TEST_F(ECS, GetComponent)
{
    Pls::Entity entity = Pls::ECS::CreateEntity();
    EXPECT_TRUE(Pls::ECS::GetComponent<int>(entity) == nullptr);
    Pls::ECS::AddComponent<int>(entity);
    EXPECT_TRUE(Pls::ECS::GetComponent<int>(entity) != nullptr);
}
TEST_F(ECS, GetComponentRetained)
{
    Pls::Entity entity = Pls::ECS::CreateEntity();
    EXPECT_TRUE(Pls::ECS::GetComponent<int>(entity) == nullptr);
    Pls::ECS::AddComponent<int>(entity) = 5;
    EXPECT_TRUE(*Pls::ECS::GetComponent<int>(entity) == 5);
}
TEST_F(ECS, HasComponent)
{
    Pls::Entity entity = Pls::ECS::CreateEntity();
    EXPECT_FALSE(Pls::ECS::HasComponent<int>(entity));
    Pls::ECS::AddComponent<int>(entity);
    EXPECT_TRUE(Pls::ECS::HasComponent<int>(entity));
}
TEST_F(ECS, RemoveComponent)
{
    Pls::Entity entity = Pls::ECS::CreateEntity();
    Pls::ECS::AddComponent<int>(entity);
    EXPECT_TRUE(Pls::ECS::HasComponent<int>(entity));
    Pls::ECS::RemoveComponent<int>(entity);
    EXPECT_FALSE(Pls::ECS::HasComponent<int>(entity));
}
TEST_F(ECS, Clear)
{
    // TODO
}
TEST_F(ECS, GetComponentCount)
{
    constexpr int ITERATIONS = 23;
    for (int i = 0; i < ITERATIONS; ++i)
    {
        Pls::Entity e = Pls::ECS::CreateEntity();
        Pls::ECS::AddComponent<Int>(e);
    }

    EXPECT_EQ(Pls::ECS::GetComponentsCount<Int>(), ITERATIONS);
}
TEST_F(ECS, RegisterSystem)
{
    class TestRoutine : public Pls::SystemRoutine<void, int>
    {
      public:
        using Pls::SystemRoutine<void, int>::SystemRoutine;
        void execute(Pls::SceneView<int>&) override
        {
        }

    };
    TestRoutine routine(nullptr);
    Pls::ECS::RegisterSystem<void, int>(&routine);
    Pls::ECS::RegisterSystem(&routine);
}
TEST_F(ECS, UpdateSystem)
{
    class TestRoutine : public Pls::SystemRoutine<int, int>
    {
      public:
        using Pls::SystemRoutine<int, int>::SystemRoutine;
        void execute(Pls::SceneView<int>&) override
        {
            ++(*baseSystem);
        }

    };
    int baseSystem = 0;
    TestRoutine routine(&baseSystem);
    Pls::ECS::RegisterSystem(&routine);
    Pls::ECS::UpdateSystems();
    EXPECT_EQ(baseSystem, 1);
}

TEST_F(ECS, TestRunWithObject)
{
    // Define Systems and Routines
    class TestRoutine : public Pls::SystemRoutine<int, int>
    {
      public:
        using Pls::SystemRoutine<int, int>::SystemRoutine;
        void execute(Pls::SceneView<int>&) override
        {
            ++(*baseSystem);
        }

    };
    int baseSystem = 0;
    TestRoutine routine(&baseSystem);

    // Create Entities
    Pls::Entity e = Pls::ECS::CreateEntity();
    Pls::ECS::AddComponent<int>(e);

    // Register Systems
    Pls::ECS::RegisterSystem(&routine);

    // Update Systems
    Pls::ECS::UpdateSystems();
    EXPECT_EQ(baseSystem, 1);
}

TEST_F(ECS, TestRunWithObjectWithRuntimeComponentAdd)
{
    // Define Systems and Routines
    class TestRoutine : public Pls::SystemRoutine<int, int>
    {
      public:
        using Pls::SystemRoutine<int, int>::SystemRoutine;
        void execute(Pls::SceneView<int>& sv) override
        {
            for (Pls::EntityPack<int>& component : sv)
            {
                ++(*baseSystem);
            }
        }

    };
    int timesRun = 0;
    TestRoutine routine(&timesRun);

    // Create Entities
    Pls::Entity e = Pls::ECS::CreateEntity();
    Pls::Entity e2 = Pls::ECS::CreateEntity();
    Pls::ECS::AddComponent<int>(e);

    // Register Systems
    Pls::ECS::RegisterSystem(&routine);

    // Update Systems
    Pls::ECS::UpdateSystems();
    EXPECT_EQ(timesRun, 1);

    // Then Add a component
    Pls::ECS::AddComponent<int>(e2);

    // Update Systems again
    Pls::ECS::UpdateSystems();
    EXPECT_EQ(timesRun, 3);
}

TEST_F(ECS, TestRunWithObjectWithRuntimeComponentRemoval)
{
    // Define Systems and Routines
    class TestRoutine : public Pls::SystemRoutine<int, int>
    {
      public:
        using Pls::SystemRoutine<int, int>::SystemRoutine;
        void execute(Pls::SceneView<int>& sv) override
        {
            for (const Pls::EntityPack<int>& component : sv)
            {
                ++(*baseSystem);
            }
        }

    };
    int timesRun = 0;
    TestRoutine routine(&timesRun);

    // Create Entities
    Pls::Entity e = Pls::ECS::CreateEntity();
    Pls::Entity e2 = Pls::ECS::CreateEntity();
    Pls::ECS::AddComponent<int>(e);
    Pls::ECS::AddComponent<int>(e2);

    // Register Systems
    Pls::ECS::RegisterSystem(&routine);

    // Update Systems
    Pls::ECS::UpdateSystems();
    EXPECT_EQ(timesRun, 2);

    // Then remove a component
    Pls::ECS::RemoveComponent<int>(e2);
    
    // Update Systems
    Pls::ECS::UpdateSystems();
    EXPECT_EQ(timesRun, 3);

    // Then remove a component
    Pls::ECS::RemoveComponent<int>(e);
    
    // Update Systems
    Pls::ECS::UpdateSystems();
    EXPECT_EQ(timesRun, 3);
}

TEST_F(ECS, TestRunWithObjectWithRuntimeEntityRemoval)
{
    // Define Systems and Routines
    class TestRoutine : public Pls::SystemRoutine<int, int>
    {
      public:
        using Pls::SystemRoutine<int, int>::SystemRoutine;
        void execute(Pls::SceneView<int>& sv) override
        {
            for (const Pls::EntityPack<int>& component : sv)
            {
                ++(*baseSystem);
            }
        }

    };
    int timesRun = 0;
    TestRoutine routine(&timesRun);

    // Create Entities
    Pls::Entity e = Pls::ECS::CreateEntity();
    Pls::Entity e2 = Pls::ECS::CreateEntity();
    Pls::ECS::AddComponent<int>(e);
    Pls::ECS::AddComponent<int>(e2);

    // Register Systems
    Pls::ECS::RegisterSystem(&routine);

    // Update Systems
    Pls::ECS::UpdateSystems();
    EXPECT_EQ(timesRun, 2);

    // Then remove an entity
    Pls::ECS::DestroyEntity(e2);
    
    // Update Systems
    Pls::ECS::UpdateSystems();
    EXPECT_EQ(timesRun, 3);

    // Then remove a component
    Pls::ECS::DestroyEntity(e);
    
    // Update Systems
    Pls::ECS::UpdateSystems();
    EXPECT_EQ(timesRun, 3);
}

TEST_F(ECS, TestRunWithObjectWithRuntimeMix)
{
    // Define Systems and Routines
    class TestRoutine : public Pls::SystemRoutine<int, int>
    {
      public:
        using Pls::SystemRoutine<int, int>::SystemRoutine;
        void execute(Pls::SceneView<int>& sv) override
        {
            for (const Pls::EntityPack<int>& component : sv)
            {
                ++(*baseSystem);
            }
        }

    };
    int timesRun = 0;
    TestRoutine routine(&timesRun);

    // Create Entities
    Pls::Entity e = Pls::ECS::CreateEntity();
    Pls::Entity e2 = Pls::ECS::CreateEntity();
    Pls::Entity e3 = Pls::ECS::CreateEntity();
    Pls::Entity e4 = Pls::ECS::CreateEntity();
    Pls::Entity e5 = Pls::ECS::CreateEntity();
    Pls::Entity e6 = Pls::ECS::CreateEntity();
    Pls::ECS::AddComponent<int>(e);
    Pls::ECS::AddComponent<int>(e2);

    // Register Systems
    Pls::ECS::RegisterSystem(&routine);

    // Update Systems
    Pls::ECS::UpdateSystems();
    EXPECT_EQ(timesRun, 2);

    // Add
    Pls::ECS::AddComponent<int>(e3);
    Pls::ECS::AddComponent<int>(e4);
    Pls::ECS::AddComponent<int>(e5);
    // Then remove an entity
    Pls::ECS::DestroyEntity(e2);    
    // Update Systems
    Pls::ECS::UpdateSystems();
    EXPECT_EQ(timesRun, 6);

    // Remove and Add    
    Pls::ECS::RemoveComponent<int>(e);
    Pls::ECS::RemoveComponent<int>(e3);
    Pls::ECS::RemoveComponent<int>(e4);
    Pls::ECS::AddComponent<int>(e6); 
    // Update Systems
    Pls::ECS::UpdateSystems();
    EXPECT_EQ(timesRun, 8);

    // Then remove a component
    Pls::ECS::DestroyEntity(e6);
    
    // Update Systems
    Pls::ECS::AddComponent<int>(e); 
    Pls::ECS::UpdateSystems();
    EXPECT_EQ(timesRun, 10);
}