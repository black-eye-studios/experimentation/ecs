// Precompiled Header
#include "pch.h"
// Project Headers
#include "SharedTypes.h"
#include "../ECS/SparseSet.h"
#include "../ECS/Utility.h"
#include "../ECS/TypeId.h"
#include "../ECS/ECS.h"

TEST(EntityPackList, ConstructEmptyView)
{
    Pls::ComponentStore store;
    Pls::EntityPackList<int> epl(store);
}
TEST(EntityPackList, ConstructFilledView)
{
    Pls::ComponentStore store;
    store.Add<int>(Pls::Entity(0));
    store.Add<int>(Pls::Entity(1));
    store.Add<int>(Pls::Entity(2));
    Pls::EntityPackList<int> epl(store);
}
TEST(EntityPackList, ConstructMultiType)
{    
    Pls::ComponentStore store;
    store.Add<int>(Pls::Entity(0));
    store.Add<int>(Pls::Entity(1));
    store.Add<int>(Pls::Entity(2));
    store.Add<bool>(Pls::Entity(0));
    store.Add<bool>(Pls::Entity(1));
    store.Add<bool>(Pls::Entity(2));
    store.Add<float>(Pls::Entity(0));
    store.Add<float>(Pls::Entity(1));
    store.Add<float>(Pls::Entity(2));
    Pls::EntityPackList<int, bool, float> epl(store);
}
TEST(EntityPackList, Iterator)
{
    Pls::ComponentStore store;
    store.Add<int>(Pls::Entity(0)) = 0;
    store.Add<int>(Pls::Entity(1)) = 1;
    store.Add<int>(Pls::Entity(2)) = 2;
    Pls::EntityPackList<int> epl(store);
    
    EXPECT_EQ((epl.begin()    )->Get<int>(), 0);
    EXPECT_EQ((epl.begin() + 1)->Get<int>(), 1);
    EXPECT_EQ((epl.begin() + 2)->Get<int>(), 2);
}
TEST(EntityPackList, ConstIterator)
{
    Pls::ComponentStore store;
    store.Add<int>(Pls::Entity(0)) = 0;
    store.Add<int>(Pls::Entity(1)) = 1;
    store.Add<int>(Pls::Entity(2)) = 2;
    Pls::EntityPackList<int> epl(store);
    
    EXPECT_EQ((epl.cbegin()    )->Get<int>(), 0);
    EXPECT_EQ((epl.cbegin() + 1)->Get<int>(), 1);
    EXPECT_EQ((epl.cbegin() + 2)->Get<int>(), 2);
}
TEST(EntityPackList, RangeBasedForLoop)
{
    Pls::ComponentStore store;
    store.Add<int>(Pls::Entity(0)) = 0;
    store.Add<int>(Pls::Entity(1)) = 1;
    store.Add<int>(Pls::Entity(2)) = 2;
    Pls::EntityPackList<int> epl(store);
    
    int value = 0;
    for (const auto& pack : epl)
    {
        EXPECT_EQ(pack.Get<int>(), value++);
    }
}