// Precompiled Header
#include "pch.h"
// Standard Library
#include <array>
// Project Headers
#include "SharedTypes.h"
#include "../ECS/Range.h"

TEST(Range, Integers)
{
    constexpr int MIN = 1;
    constexpr int MAX = 6;
    Pls::Range range(MIN, MAX);

    int counter = MIN;
    for (int val = range.begin(); val != range.end(); ++val)
    {
        EXPECT_EQ(val, counter++);
    }
}
TEST(Range, STLContainers)
{
    constexpr size_t SIZE = 5;
    const std::array<int, SIZE> ARRAY = { 1, 2, 3, 4, 5 };

    Pls::Range range(ARRAY.begin(), ARRAY.end());

    int counter = 1;
    for (const auto& val : range)
    {
        EXPECT_EQ(val, counter++);
    }
}

TEST(Range, STLContainersExplicitConvenienceFunction)
{
    constexpr size_t SIZE = 5;
    const std::array<int, SIZE> ARRAY = { 1, 2, 3, 4, 5 };

    Pls::Range<std::array<int, SIZE>::const_iterator> range(ARRAY.begin(), ARRAY.end());

    int counter = 1;
    for (const auto& val : range)
    {
        EXPECT_EQ(val, counter++);
    }
}

TEST(Range, STLContainersImplicitConvenienceFunction)
{
    constexpr size_t SIZE = 5;
    const std::array<int, SIZE> ARRAY = { 1, 2, 3, 4, 5 };

    Pls::Range range(ARRAY.begin(), ARRAY.end());

    int counter = 1;
    for (const auto& val : range)
    {
        EXPECT_EQ(val, counter++);
    }
}