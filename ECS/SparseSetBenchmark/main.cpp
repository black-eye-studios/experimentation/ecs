#include <iostream>
#include <array>
#include <vector>
#include <list>
#include <random>
#include <chrono>
#include <algorithm>

#include "../ECS/SparseSet.h"

class Int {};

int main()
{
    // Constants
    constexpr size_t OBJECT_COUNT = 60000;

    // Initialize Random Engine
    std::default_random_engine eng;
    std::uniform_int_distribution<size_t> dist { 0, OBJECT_COUNT - 1 };

    // Generate a number of accesses
    constexpr size_t RANDOM_ACCESS_COUNT = OBJECT_COUNT;
    std::vector<size_t> accessIndices;
    for (size_t i = 0; i < RANDOM_ACCESS_COUNT; ++i)
    {
        accessIndices.emplace_back(dist(eng));
    }

    // Start doing benchmarks
    std::cout << "======================================\n";
    std::cout << "Array Access Benchmark\n";
    std::cout << "======================================" << std::endl;
    auto start = std::chrono::high_resolution_clock::now();
    std::array<int, OBJECT_COUNT> objectsArr = {0};
    for (size_t i = 0; i < OBJECT_COUNT; ++i)
    {
        objectsArr[i] = 123;
    }
    std::chrono::duration<double> arrFillTime = std::chrono::high_resolution_clock::now() - start;
    std::cout << "Insertion Time: " << arrFillTime.count() << std::endl;
    start = std::chrono::high_resolution_clock::now();
    for (size_t i = 0; i < accessIndices.size(); ++i)
    {
        int a = objectsArr[i];
    }
    std::chrono::duration<double> arrAccessTime = std::chrono::high_resolution_clock::now() - start;
    std::cout << "Random Access Time: " << arrAccessTime.count() << std::endl;
    start = std::chrono::high_resolution_clock::now();
    int total = 0;
    std::for_each(objectsArr.begin(), objectsArr.end(), [&total](auto i) { total += i; });
    std::chrono::duration<double> arrSeqAccessTime = std::chrono::high_resolution_clock::now() - start;
    std::cout << "Sequential Access Time: " << arrSeqAccessTime.count() << std::endl;

    std::cout << "======================================\n";
    std::cout << "Vector Access Benchmark\n";
    std::cout << "======================================" << std::endl;
    std::vector<int> objectsVec;
    for (size_t i = 0; i < OBJECT_COUNT; ++i)
    {
        objectsVec.emplace_back(i);
    }    
    std::chrono::duration<double> vecFillTime = std::chrono::high_resolution_clock::now() - start;
    std::cout << "Insertion Time: " << vecFillTime.count() << std::endl;
    start = std::chrono::high_resolution_clock::now();
    for (size_t i = 0; i < accessIndices.size(); ++i)
    {
        int a = objectsVec[i];
    }
    std::chrono::duration<double> vecAccessTime = std::chrono::high_resolution_clock::now() - start;
    std::cout << "Random Access Time: " << vecAccessTime.count() << std::endl;
    start = std::chrono::high_resolution_clock::now();
    total = 0;
    std::for_each(objectsVec.begin(), objectsVec.end(), [&total](auto i) { total += i; });
    std::chrono::duration<double> vecSeqAccessTime = std::chrono::high_resolution_clock::now() - start;
    std::cout << "Sequential Access Time: " << vecSeqAccessTime.count() << std::endl;

    std::cout << "======================================\n";
    std::cout << "List Access Benchmark\n";
    std::cout << "======================================" << std::endl;
    std::list<int> objectsList;
    for (size_t i = 0; i < OBJECT_COUNT; ++i)
    {
        objectsList.emplace_back(i);
    }    
    std::chrono::duration<double> listFillTime = std::chrono::high_resolution_clock::now() - start;
    std::cout << "Insertion Time: " << listFillTime.count() << std::endl;
    std::cout << "Random Access Time: DNF" << std::endl;
    start = std::chrono::high_resolution_clock::now();
    total = 0;
    std::for_each(objectsList.begin(), objectsList.end(), [&total](auto i) { total += i; });
    std::chrono::duration<double> listSeqAccessTime = std::chrono::high_resolution_clock::now() - start;
    std::cout << "Sequential Access Time: " << listSeqAccessTime.count() << std::endl;

    std::cout << "======================================\n";
    std::cout << "Sparse Set Access Benchmark\n";
    std::cout << "======================================" << std::endl;
    start = std::chrono::high_resolution_clock::now();
    Pls::SparseSet<int> objectsSet;
    for (size_t i = 0; i < OBJECT_COUNT; ++i)
    {
        objectsSet.insert(i) = 123;
    }
    std::chrono::duration<double> setFillTime = std::chrono::high_resolution_clock::now() - start;
    std::cout << "Insertion Time: " << setFillTime.count() << std::endl;

    start = std::chrono::high_resolution_clock::now();
    for (size_t i = 0; i < accessIndices.size(); ++i)
    {
        int a = objectsSet[i];
    }
    std::chrono::duration<double> setAccessTime = std::chrono::high_resolution_clock::now() - start;
    std::cout << "Random Access Time: " << setAccessTime.count() << std::endl;

    start = std::chrono::high_resolution_clock::now();
    total = 0;
    std::for_each(objectsSet.begin(), objectsSet.end(), [&total](auto i) { total += i;  });
    std::chrono::duration<double> setSeqAccessTime = std::chrono::high_resolution_clock::now() - start;
    std::cout << "Sequential Access Time: " << setSeqAccessTime.count() << std::endl;
}