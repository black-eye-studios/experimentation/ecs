#pragma once

#include <unordered_map> // std::unordered_map

#include "TypeId.h"           // Pls::TypeId
#include "SparseSet.h"        // Pls::SparseSet
#include "Entity.h"           // Pls::Entity
#include "Range.h"            // Pls::Range

namespace Pls
{
    /********************************************************************************//*!
    @brief    Responsible for managing storage for different types of Components stored
              in SparseSets.

              Note, there is a number of uses of reinterpret_cast in order to avoid
              the performance cost of dynamic_cast. This is ok since the type is
              guranteed already and we do not need the checks provided by dynamic_cast.
    *//*********************************************************************************/
    class ComponentStore final
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Type Definition                                                             */
        /*-----------------------------------------------------------------------------*/
        using SparseSetContainer   = SparseSetBase<Entity::MAX>;
        using size_type            = SparseSetContainer::size_type;
        using dense_const_iterator = SparseSetContainer::dense_const_iterator;
        /*-----------------------------------------------------------------------------*/
        /* Constructors/Destructors                                                    */
        /*-----------------------------------------------------------------------------*/
        explicit ComponentStore() = default;
        ~ComponentStore();

        // Disallow copying and moving
        ComponentStore(const ComponentStore&) = delete;
        ComponentStore(ComponentStore&&) = delete;        
        
        /*-----------------------------------------------------------------------------*/
        /* Usage Functions - Overall                                                   */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Gets the number of Components of the specified type that are active.

        @tparam       Component
                Type of the Component to get a count of.

        @return   Number of Components of the specified type that are active.
        *//*****************************************************************************/
        template<typename Component>
        size_type GetCount() const;

        /*-----------------------------------------------------------------------------*/
        /* Usage Functions - Per Component                                             */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Gets a Component to the specified entity.

        @tparam       Component
                Type of the Component to get.
        @param[in]    entity
                Read-only reference to the Entity to retrieve the Component for.

        @return   Pointer to the Component of the type specified. Nullptr if none exists.
        *//*****************************************************************************/
        template<typename Component>
        Component* Get(const Entity& entity) const;
        /****************************************************************************//*!
        @brief    Checks if the specified Entity has a specified Component.
        
        @throws    std::out_of_range
                If the entity does not exist to begin with.

        @tparam       Component
                Type of the Component to check.
        @param[in]    entity
                Read-only reference to the Entity to retrieve the Component for.

        @return   True if the specified Entity has the specified Component.
        *//*****************************************************************************/
        template<typename Component>
        bool Has(const Entity& entity) const;
        /****************************************************************************//*!
        @brief    Adds a Component to the specified Entity.

        @throws    std::invalid_argument
                If the Component already exists.

        @tparam       Component
                Type of the Component to add.
        @param[in]    entity
                Read-only reference to the Entity to add the Component to.

        @return   Reference to the Component of the type specified that was added.
        *//*****************************************************************************/
        template<typename Component>
        Component& Add(const Entity& entity);
        /****************************************************************************//*!
        @brief    Removes a Component from the specified entity.

        @throws    std::invalid_argument
                If the Component does not exist to begin with.
                
        @tparam       Component
                Type of the Component to remove.
        @param[in]    entity
                Read-only reference to the Entity to remove a Component from.
        *//*****************************************************************************/
        template<typename Component>
        void Remove(const Entity& entity);
        /****************************************************************************//*!
        @brief    Removes all Components from the specified entity.

        @param[in]    entity
                Read-only reference to the Entity to remove a Component from.
        *//*****************************************************************************/
        void RemoveAll(const Entity& entity);
        /****************************************************************************//*!
        @brief    Retrieves an unsorted range of IDs to Entities that has the specified
                  component.
                  
        @tparam       Component
                Type of the Component to get a list of Entities that have it.

        @return   A Range object that defines a list of Entity IDs that contain
        *//*****************************************************************************/
        template<typename Component>
        Range<dense_const_iterator> GetEntities() const;
        /****************************************************************************//*!
        @brief    Clears all component storage.
        *//*****************************************************************************/
        void Clear();

      private:
        std::unordered_map<TypeId::InternalHashType, SparseSetContainer*> sparseSetMaps;    };
    template<typename Component>
    inline typename ComponentStore::size_type ComponentStore::GetCount() const
    {
        // Get the type ID
        constexpr TypeId::InternalHashType TYPE_ID = TypeId::Get<Component>().GetId();

        // No component if there's no storage to begin with
        if (sparseSetMaps.find(TYPE_ID) == sparseSetMaps.end())
        {
            return 0;
        }

        // Otherwise, it exists so let's get the size
        auto componentSparseSet = reinterpret_cast<SparseSet<Component>*>(sparseSetMaps.at(TYPE_ID));
        return componentSparseSet->size();
    }
    template<typename Component>
    inline Component* ComponentStore::Get(const Entity& entity) const
    {
        // Get the type ID
        constexpr TypeId::InternalHashType TYPE_ID = TypeId::Get<Component>().GetId();

        // No component if there's no storage to begin with
        if (sparseSetMaps.find(TYPE_ID) == sparseSetMaps.end())
        {
            return nullptr;
        }
        
        // Retrieve the element
        auto componentSparseSet = reinterpret_cast<SparseSet<Component>*>(sparseSetMaps.at(TYPE_ID));

        try
        {
            return &componentSparseSet->at(entity.GetID());
        }
        catch (std::out_of_range)
        {
            // Component does not exist
            return nullptr;
        }
    }
    template<typename Component>
    inline bool ComponentStore::Has(const Entity& entity) const
    {
        // Get the type ID
        constexpr TypeId::InternalHashType TYPE_ID = TypeId::Get<Component>().GetId();

        // No component if there's no storage to begin with
        if (sparseSetMaps.find(TYPE_ID) == sparseSetMaps.end())
        {
            return false;
        }
        
        auto componentSparseSet = reinterpret_cast<SparseSet<Component>*>(sparseSetMaps.at(TYPE_ID));
        return componentSparseSet->contains(entity.GetID());
    }
    template<typename Component>
    inline Component& ComponentStore::Add(const Entity& entity)
    {
        //static_assert(std::is_same<Component, void>::value, "Component specified cannot be void!");

        // Get the type ID
        constexpr TypeId::InternalHashType TYPE_ID = TypeId::Get<Component>().GetId();

        // Create the sparse set if it doesn't exist yet
        if (sparseSetMaps.find(TYPE_ID) == sparseSetMaps.end())
        {
            sparseSetMaps[TYPE_ID] = new SparseSet<Component>();
        }

        // Insert the element
        auto componentSparseSet = reinterpret_cast<SparseSet<Component>*>(sparseSetMaps.at(TYPE_ID));
        Component& component = componentSparseSet->insert(entity.GetID()); // throws std::invalid_argument on failure

        return component;
    }
    template<typename Component>
    inline void ComponentStore::Remove(const Entity& entity)
    {
        // Get the type ID
        constexpr auto TYPE_ID = TypeId::Get<Component>().GetId();
        
        // No component if there's no storage to begin with
        if (sparseSetMaps.find(TYPE_ID) == sparseSetMaps.end())
        {
            throw std::invalid_argument("Attempted to remove a Component that does not exist!");
        }

        // Remove the element
        auto componentSparseSet = reinterpret_cast<SparseSet<Component>*>(sparseSetMaps.at(TYPE_ID));
        componentSparseSet->erase(entity.GetID());
    }
    template<typename Component>
    inline Range<typename ComponentStore::dense_const_iterator> ComponentStore::GetEntities() const
    {
        // Get the type ID
        constexpr auto TYPE_ID = TypeId::Get<Component>().GetId();

        // No component if there's no storage to begin with
        if (sparseSetMaps.find(TYPE_ID) == sparseSetMaps.end())
        {
            return Range<dense_const_iterator>();
        }

        // Create the Range object
        auto componentSparseSet = reinterpret_cast<SparseSet<Component>*>(sparseSetMaps.at(TYPE_ID));
        return Range<dense_const_iterator>
        (
            componentSparseSet->dense_cbegin(), 
            componentSparseSet->dense_cend()
        );
    }
}
