#pragma once

namespace Pls
{
    /********************************************************************************//*!
    @brief    Defines a read only half open range of any type. This can be used with any
              type that is incrementable or decrementable. Candidates include iterators, 
              numerical types and more. Note that for-range loops with Range only work
              with iterator-like types.

              In the scenario that you need an "empty" range, constructing a range
              without any parameters provides that.

              Note that when used with Iterators, Ranges become invalidated when said
              Iterators also become invalidated.
    *//*********************************************************************************/
    template<typename T>
    class Range
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Constructors/Destructors                                                    */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Constructs an empty Range. Calling any of the iterator functions on an
                  empty range results in a std::out_of_range exception.
        *//*****************************************************************************/
        Range();        
        /****************************************************************************//*!
        @brief    Constructs a Range that describes a range of values.
        
        @param[in]    begin
                The first element of the range. If T is an iterator, this would be where
                a std::begin() call is present.
        @param[in]    end
                The one past last element of the range. If T is an iterator, this would 
                be where a std::end() call is present.
        *//*****************************************************************************/
        Range(const T& begin, const T& end);

        template<typename Container>
        Range(const Container& container);
        
        /*-----------------------------------------------------------------------------*/
        /* Iterator Functions                                                          */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Retrieves the first element of the Range.
                
        @throws    std::out_of_range
                If the Range is an empty Range.

        @return   Read only reference to the first element.
        *//*****************************************************************************/
        inline const T& begin() const;
        /****************************************************************************//*!
        @brief    Retrieves the last element of the Range.
                
        @throws    std::out_of_range
                If the Range is an empty Range.

        @return   Read only reference to the last element.
        *//*****************************************************************************/
        inline const T& end() const;
        /*-----------------------------------------------------------------------------*/
        /* Usage Functions                                                             */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Retrieves the size of the Range.

        @return   Size of the Range.
        *//*****************************************************************************/
        inline size_t   size() const;
        /****************************************************************************//*!
        @brief    Checks if the Range is an empty Range.

        @return   True if the Range is empty.
        *//*****************************************************************************/
        inline bool     empty() const;

      private:
        const T BEGIN;
        const T END;
        const bool EMPTY;
    };

    /*---------------------------------------------------------------------------------*/
    /* Range Template Argument Deduction Guides                                        */
    /*---------------------------------------------------------------------------------*/
    template<typename Container>
    Range(const Container&) -> Range<typename Container::const_iterator>;


    /*---------------------------------------------------------------------------------*/
    /* Range Template Functions Definitions                                            */
    /*---------------------------------------------------------------------------------*/
    template<typename T>
    inline Range<T>::Range()
    : BEGIN { }
    , END   { }
    , EMPTY { true }
    {}
    template<typename T>
    inline Range<T>::Range(const T& begin, const T& end)
    : BEGIN { begin }
    , END   { end }
    , EMPTY { false }
    {}
    template<typename T>
    inline const T& Range<T>::begin() const
    {
        if (EMPTY) throw std::out_of_range("Attempted to obtain iterators to an empty range!");
        return BEGIN;
    }
    template<typename T>
    inline const T& Range<T>::end() const
    {
        if (EMPTY) throw std::out_of_range("Attempted to obtain iterators to an empty range!");
        return END;
    }
    template<typename T>
    inline size_t Range<T>::size() const
    {
        // Special case for empty ranges
        if (EMPTY)
        {
            return 0;
        }

        return END - BEGIN;
    }
    template<typename T>
    inline bool Range<T>::empty() const
    {
        return EMPTY;
    }
    template<typename T>
    template<typename Container>
    inline Range<T>::Range(const Container& container)
    : Range(container.begin(), container.end())
    {}
}