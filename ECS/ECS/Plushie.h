#pragma once

#if defined _WIN32 || defined __CYGWIN__ || defined _MSC_VER
#    define PLS_EXPORT __declspec(dllexport)
#    define PLS_IMPORT __declspec(dllimport)
#elif defined __GNUC__ && __GNUC__ >= 4
#    define PLS_EXPORT __attribute__((visibility("default")))
#    define PLS_IMPORT __attribute__((visibility("default")))
#else /* Unsupported compiler */
#    define PLS_EXPORT
#    define PLS_IMPORT
#endif

#ifndef PLS_API
#   if defined PLS_API_EXPORT
#       define PLS_API PLS_EXPORT
#   elif defined PLS_API_IMPORT
#       define PLS_API PLS_IMPORT
#   else /* No API */
#       define PLS_API
#   endif
#endif