#include "ECS.h"

namespace Pls
{
    /*---------------------------------------------------------------------------------*/
    /* Public Static Member Definitions                                                */
    /*---------------------------------------------------------------------------------*/
    const ECS::EntitiesView         ECS::Entities(ECS::entities);

    /*---------------------------------------------------------------------------------*/
    /* Private Static Member Definitions                                               */
    /*---------------------------------------------------------------------------------*/
    EntityManager                                                     ECS::entities;
    ComponentStore                                                    ECS::components;
    std::vector<SystemRoutineBase*>                                   ECS::systemsRoutines;
    std::unordered_map<TypeId::InternalHashType, EntityPackListBase*> ECS::cachedEntityLists;
    std::unordered_map<TypeId::InternalHashType, SceneViewBase*>      ECS::cachedSceneViews;
    std::unordered_map<TypeId::InternalHashType, ECS::EntityEvent>    ECS::onComponentAddedEvent;
    std::unordered_map<TypeId::InternalHashType, ECS::EntityEvent>    ECS::onComponentRemovedEvent;
    ECS::EntityEvent                                                  ECS::onEntityDestroyedEvent;

    /*---------------------------------------------------------------------------------*/
    /* Lifecycle Functions                                                             */
    /*---------------------------------------------------------------------------------*/
    void ECS::UpdateSystems()
    {
        // ComponentStore should have "events" that trigger when components are added or
        // removed which will update the SceneViews.


        // Iterate through all systems to update them and pass them the appropriate SceneViews
        for (const auto& routine : systemsRoutines)
        {
            // TODO: Potential Optimization
            // systemsRoutines should be stored as a std::pair<SceneViewBase*, SystemRoutine>
            // This allows us to easily pass the SceneView* to the SystemRoutine and quickly.
        
            // Pass in the required SceneView based on the ComponentBitFlag
            auto sceneView = cachedSceneViews[routine->GetCombinationId().GetId()];
            routine->Execute(sceneView);
        }
    }
    void ECS::Reset()
    {
        entities.Clear();
        components.Clear();
        systemsRoutines.clear();

        // Delete all cached entity lists and corresponding scene views
        for (const auto& sceneViewPair : cachedEntityLists)
        {
            delete sceneViewPair.second;
        }
        cachedEntityLists.clear();
        for (const auto& sceneViewPair : cachedSceneViews)
        {
            delete sceneViewPair.second;
        }
        cachedSceneViews.clear();

        // Clear all events
        onComponentAddedEvent.clear();
        onComponentRemovedEvent.clear();
        onEntityDestroyedEvent.Clear();
    }
    /*---------------------------------------------------------------------------------*/
    /* Usage Functions - Entity                                                        */
    /*---------------------------------------------------------------------------------*/
    Entity ECS::CreateEntity()
    {
        return entities.Create();
    }
    void ECS::DestroyEntity(const Entity& entity)
    {
        // Destroy the entity
        entities.Destroy(entity);

        // Invalidate all components
        components.RemoveAll(entity);

        // Raise removal event
        onEntityDestroyedEvent.Invoke(entity);
    }
}