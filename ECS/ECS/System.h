#pragma once

#include "SceneView.h"

namespace Pls
{
    /********************************************************************************//*!
    @brief    Lightweight object so that we can box/unbox derived templated 
              SystemRoutines.
    *//*********************************************************************************/
    class SystemRoutineBase
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Abstract Functions                                                          */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Abstract function call that is to be overriden in SystemRoutine<T>,
                  the derived version of this class. This is exposed so that 
                  SystemRoutines can be executed without requiring the specific type.
        *//*****************************************************************************/
        virtual void Execute(void*) = 0;

        /*-----------------------------------------------------------------------------*/
        /* Getter Functions                                                            */
        /*-----------------------------------------------------------------------------*/
        inline TypeId GetCombinationId() const { return componentComboId; }

      protected:
        /*-----------------------------------------------------------------------------*/
        /* Constructors/Destructors                                                    */
        /*-----------------------------------------------------------------------------*/
        inline SystemRoutineBase(TypeId typeId) : componentComboId{ typeId } {}
        virtual ~SystemRoutineBase() = default;

        /*-----------------------------------------------------------------------------*/
        /* Const Data Members                                                          */
        /*-----------------------------------------------------------------------------*/
        const TypeId componentComboId;
    };

    /********************************************************************************//*!
    @brief    Describes a System Routine functor where routine behaviour can be defined.
    
    @tparam       BaseSystem
            Type of the Base System that the SystemRoutine is for.
    @tparam       Components
            Varadic template argument pack of Components that this SystemRoutine is
            designed to operate on.
    *//*********************************************************************************/
    template<typename BaseSystem, typename ... Components>
    class SystemRoutine : public SystemRoutineBase
    {
       public:
        /*-----------------------------------------------------------------------------*/
        /* Constructors/Destructors                                                    */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Default constructor to construct a SystemRoutine.

        @param[in]    baseSystem
                Pointer to the System that owns this SystemRoutine. This allows the
                SystemRoutine to reference and interact with the System.
        *//*****************************************************************************/
        SystemRoutine(BaseSystem* baseSystem);

        /*-----------------------------------------------------------------------------*/
        /* Usage Functions                                                             */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Wraps up the actual call, there is relay of pointers to mask the 
                  actual type.
        *//*****************************************************************************/
        void Execute(void* sv) override;

      protected:
        /****************************************************************************//*!
        @brief    A derived SystemRoutine would overload this, virtual dispatch cost 
                  should be minimal as it's only 1 call per SystemRoutine.
        *//*****************************************************************************/
        virtual void execute(SceneView<Components ...>&) = 0; 

        BaseSystem*  baseSystem;
    };

    /********************************************************************************//*!
    @brief    Base system that all Systems can inherit from for the convenience "Routine"
              type alias.
    
    @tparam       Derived
            Type of the Derived class.
    *//*********************************************************************************/
    template<typename Derived>
    class System
    {
      public:
        /****************************************************************************//*!
        @brief    Convenience type alias that prefills in the "BaseSystem" parameter of
                  the template argument list for defining SystemRoutines.

        @tparam       Components
                Varadic template argument pack of Components that the SystemRoutine is
                designed to operate on.
        *//*****************************************************************************/
        template<typename ... Components>
        using Routine = SystemRoutine<Derived, Components ...>;
    };
    
    /*---------------------------------------------------------------------------------*/
    /* SystemRoutine Template Functions Definitions                                    */
    /*---------------------------------------------------------------------------------*/
    template<typename BaseSystem, typename ...Components>
    inline SystemRoutine<BaseSystem, Components ...>::SystemRoutine(BaseSystem* baseSystem)
    : SystemRoutineBase { TypeId::Get<Components ...>() }
    , baseSystem { baseSystem }
    {}
    template<typename BaseSystem, typename ...Components>
    inline void SystemRoutine<BaseSystem, Components ...>::Execute(void* sv)
    {
        auto sceneViewPtr = reinterpret_cast<SceneView<Components ...>*>(sv);
        execute(*sceneViewPtr);
    }
}