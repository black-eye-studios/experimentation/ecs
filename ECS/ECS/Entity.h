#pragma once

#include <cstdint> // uint32_t, uint16_t

namespace Pls
{
    /********************************************************************************//*!
    @brief    Lightweight object that encapsulates what an "Entity" is, a simple integer
              that contains and ID and a Version used to group Components together.
    *//*********************************************************************************/
    class Entity final
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Type Definitions                                                            */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Raw type used to represent the entire Entity object.
        *//*****************************************************************************/
        using RawType     = uint32_t;
        /****************************************************************************//*!
        @brief    Integral type used to represent the version portion of the Entity's
                  internal ID.
        *//*****************************************************************************/
        using IDType      = uint16_t;
        /****************************************************************************//*!
        @brief    Integral type used to represent the ID portion of the Entity's
                  internal ID.
                  
                  This may overflow as it is incremented, but that is ok since it is 
                  unsigned. The likelihood of this being a problem is negligible as 
                  VersionType is currently uint16_t. But worth noting here in the 
                  scenario that it is switched to a uint8_t or smaller.
        *//*****************************************************************************/
        using VersionType = uint16_t;
        /*-----------------------------------------------------------------------------*/
        /* Constants                                                                   */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    The maximum number of entities that can be stored.
        *//*****************************************************************************/
        static constexpr IDType MAX = 256;
        
        /*-----------------------------------------------------------------------------*/
        /* Constructors                                                                */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Constructs an Entity based on the specified ID and Version.

        @param[in]    id
                The ID of the actual Entity that this Entity object represents.
        @param[in]    version
                The Version of the actual Entity that this Entity object represents.
        *//*****************************************************************************/
        inline Entity(IDType id, VersionType version = 0) : internalId{ id, version } {}

        /*-----------------------------------------------------------------------------*/
        /* Getters                                                                     */
        /*-----------------------------------------------------------------------------*/
        inline VersionType GetVersion() const { return internalId.Data.Version; }
        inline IDType GetID() const { return internalId.Data.Id; }

        /*-----------------------------------------------------------------------------*/
        /* Overloaded Operators                                                        */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Checks if two Entities are the same. They are the same only if they
                  have the same raw ID; there are checks with the Version. 
                  Meaning an Entity that is created but destroyed and then created again
                  with the same ID will not be the same Entity as the pre-destroyed one.

        @param[in]    rhs
                Read only reference to the other Entity to compare with.

        @return   True if both Entites are the exact same object.
        *//*****************************************************************************/
        inline bool operator==(const Entity& rhs) const { return internalId.RawData == rhs.internalId.RawData; }
        /****************************************************************************//*!
        @brief    Checks if two Entities are different. They are the different as long
                  as their raw ID differs; there are checks with the Version. 
                  Meaning an Entity that is created but destroyed and then created again
                  with the same ID will not be the same Entity as the pre-destroyed one.

        @param[in]    rhs
                Read only reference to the other Entity to compare with.

        @return   True if both Entites are not the exact same object.
        *//*****************************************************************************/
        inline bool operator!=(const Entity& rhs) const { return internalId.RawData != rhs.internalId.RawData; }

      private:
        /*-----------------------------------------------------------------------------*/
        /* Type Definitions                                                            */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Union type that makes it easy to split the raw integral type into
                  multiple parts that represents each piece of data for the Entity.
        *//*****************************************************************************/
        union EntityRawData
        {
            /************************************************************************//*!
            @brief    Raw integral ID that encapsualtes the Version and ID data.
            *//*************************************************************************/
            RawType RawData;
            /************************************************************************//*!
            @brief    Anonymous struct that splits the RawData into two portions. The 
                      version and ID as separate members for easy access.
            *//*************************************************************************/
            struct
            {
                VersionType Version;
                IDType      Id;
            } Data;
            
            /************************************************************************//*!
            @brief    Constructor to easily construct the raw data in the constructor of
                      Entity using the EntityRawData::Data struct.
            @param[in]    id
                    The ID of the actual Entity that this Entity object represents.
            @param[in]    version
                    The Version of the actual Entity that this Entity object represents.
            *//*************************************************************************/
            inline EntityRawData(IDType id, VersionType version) : Data{ version, id } {}
        };

        /*-----------------------------------------------------------------------------*/
        /* Data Members                                                                */
        /*-----------------------------------------------------------------------------*/
        EntityRawData internalId;
    };
}