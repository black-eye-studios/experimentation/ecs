#include "EntityManager.h"

#include <stdexcept> // std::invalid_argument

namespace Pls
{
    Entity EntityManager::Create()
    {
        Entity newEntity = getAvailableFreeEntity();
        entities.push_back(newEntity);
        return newEntity;
    }

    void EntityManager::Destroy(const Entity& entity)
    {
        auto iter = std::find(entities.cbegin(), entities.cend(), entity);
        if (iter == entities.cend())
        {
            throw std::invalid_argument("Attempted to destroy Entity that does not exist!");
        }
        entities.erase(iter);

        // Add entity into the freeList
        freeList.push(entity);
    }

    void EntityManager::Clear()
    {
        entities.clear();
        freeList = {};
        back = 0;
    }
    
    Entity EntityManager::getAvailableFreeEntity()
    {
        // No previously freed elements
        if (freeList.empty())
        {
            return Entity(back++);
        }

        // Freed elements exist, so let's reuse
        Entity entity = freeList.front();
        freeList.pop();
        return Entity(entity.GetID(), entity.GetVersion() + 1);
    }
}
