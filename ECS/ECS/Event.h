#pragma once

#include <vector>     // std::vector
#include <functional> // std::function
#include <stdexcept>  // std::invalid_argument

namespace Pls
{
    /********************************************************************************//*!
     @brief    A class template that represents an a collection of function
               pointers that can be easily added to and manipulated, and have the
               function pointers called easily.

               Inspired by C#'s event and delegate system, it should be used as such.
     
     @tparam    Args
         Parameters that are needed for the Event.
    *//*********************************************************************************/
    template<typename... Args>
    class Event
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Type Definitions                                                            */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
         @brief    Type of the function that this Event will take in.
        *//*****************************************************************************/
        using DelegateFormat = void(Args ...);
        /****************************************************************************//*!
         @brief    Type of the std::function wrapper for functions of the type
                   DelegateFormat.
        *//*****************************************************************************/
        using Delegate = std::function<DelegateFormat>;
        
        /*-----------------------------------------------------------------------------*/
        /* Constructors                                                                */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
         @brief    Default Constructor
        *//*****************************************************************************/
        Event() = default;
        virtual ~Event() = default;
        
        /*-----------------------------------------------------------------------------*/
        /* Usage Functions                                                             */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
         @brief    Function to add a Delegate to this Event.

                   If attempting to add a member function of a class, lambdas/anonymous
                   functions must be used to capture the object pointer.
                   E.g.: [ptr](){} or [this](){}
         
         @throw         std::invalid_argument
             Thrown if a nullptr was provided to add into the event.
         @param[in]    delegate
             The delegate to add to this Event. Multiple of the same Delegate can be
             added to the same Event with no repercussions.
        *//*****************************************************************************/
        void AddHandler(Delegate delegate);
        /****************************************************************************//*!
         @brief    Function to remove a Delegate from this Event.
                   Unless specified, if multiple of the same Delagate exist in this Event,
                   only one of them will be removed.

                   If it does not exist in the event, nothing will happen.
         
         @param[in]    delegate
             The Delegate to remove from this Event.
         @param[in]    removeAll
             Whether or not to remove all of the same Delegates in this Event.
        *//*****************************************************************************/
        void RemoveHandler(Delegate delegate, bool removeAll = false);        
        /****************************************************************************//*!
         @brief    Function that executes all added Delegates in the Event.
         
         @param[in]    args
             Varadic argument list for the required parameters to call raise this event.
        *//*****************************************************************************/
        void Invoke(Args ... args) const;
        /****************************************************************************//*!
         @brief    Removes all added Delegates from this Event.
        *//*****************************************************************************/
        void Clear();
        
        /*-----------------------------------------------------------------------------*/
        /* Overloaded Operators                                                        */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
         @brief    Overloaded += operator that acts as a shorthand of AddHandler().

                   If attempting to add a member function of a class, lambdas/anonymous
                   functions must be used to capture the object pointer.
                   E.g.: [ptr](){} or [this](){}
         
         @throw         std::invalid_argument
             Thrown if a nullptr was provided to add into the event.
         @param[in]    delegate
             The delegate to add to this Event. Multiple of the same Delegate can be
             added to the same Event with no repercussions.
        *//*****************************************************************************/
        Event& operator+=(Delegate delegate);
        /****************************************************************************//*!
         @brief    Overloaded -= operator that acts as a shorthand of RemoveHandler().
         
         @param[in]    delegate
             The Delegate to remove from this Event.
        *//*****************************************************************************/
        Event& operator-=(Delegate delegate);
        /****************************************************************************//*!
         @brief    Overloaded () operator that acts as a shorthand of Invoke().
         
         @param[in]    args
             Varadic argument list for the required parameters to call raise this event.
        *//*****************************************************************************/
        const Event& operator()(Args ... args) const;
        
      private:
        std::vector<Delegate> delegates;
    };

    template <typename ... Args>
    void Event<Args...>::AddHandler(Delegate delegate)
    {
        // Prevent invalid delegates
        if (delegate == nullptr)
        {
            throw std::invalid_argument("Attempted to add a null delegate!");
        }
        // Add the delegate
        delegates.push_back(delegate);
    }
    template <typename ... Args>
    void Event<Args...>::RemoveHandler(Delegate delegate, bool removeAll)
    {
        // Look for the delegate and remove
        for (typename std::vector<Delegate>::iterator it = delegates.begin(); it != delegates.end();)
        {
            if (it->target<DelegateFormat>() == delegate.target<DelegateFormat>())
            {
                // Remove and set the next iterator accordingly
                it = delegates.erase(it);

                // Removed one, that's it
                if (!removeAll)
                {
                    break;
                }
            }
            else
            {
                // No match, move on to the next
                ++it;
            }
        }
    }
    template <typename ... Args>
    void Event<Args...>::Invoke(Args... args) const
    {
        // Execute all delegates
        for (auto delegate : delegates)
        {
            delegate(args ...);
        }
    }
    template <typename ... Args>
    void Event<Args...>::Clear()
    {
        // Clear all delegates
        delegates.clear();
    }
    template <typename ... Args>
    Event<Args...>& Event<Args...>::operator+=(Delegate delegate)
    {
        AddHandler(delegate);
        return *this;
    }
    template <typename ... Args>
    Event<Args...>& Event<Args...>::operator-=(Delegate delegate)
    {
        RemoveHandler(delegate);
        return *this;
    }
    template <typename ... Args>
    const Event<Args...>& Event<Args...>::operator()(Args... args) const
    {
        Invoke(args ...);
        return *this;
    }
}
