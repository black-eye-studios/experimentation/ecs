#pragma once

#include "Entity.h" // Pls::Entity
#include "ECS.h"    // Pls::ECS

namespace Pls
{
    /********************************************************************************//*!
    @brief    Object that represents an Entity with corresponding references to
              Components of the Entity with the specified Components.

    @tparam    Components
            The type of Components that the Entity that this EntityPack represents should
            contain.
    *//*********************************************************************************/
    template<typename ... Components>
    class EntityPack
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Public Constants                                                            */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Retrieves a Component of the type T from the EntityPack.
        *//*****************************************************************************/
        const Entity Entity;

        /*-----------------------------------------------------------------------------*/
        /* Constructors                                                                */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Constructs a EntityPack based on a specified Entity.

        @param[in]    entity
                Read only reference to the Entity to create this EntityPack from.
        @param[in]    components
                Varadic template argument pack of Components that form the EntityPack.

        @return   Reference to the Component of the type specified.
        *//*****************************************************************************/
        EntityPack(const Pls::Entity& entity, Components* ... components);
        
        /*-----------------------------------------------------------------------------*/
        /* Usage Functions                                                             */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Retrieves a Component of the type T from the EntityPack.

        @tparam       T
                The type of Component to retrieve. This type must be one of the types of
                Components that this EntityPack was defined with.

        @return   Reference to the Component of the type specified.
        *//*****************************************************************************/
        template<typename T>
        T& Get() const;
        /****************************************************************************//*!
        @brief    Overloaded copy assignment operator to facilitate overriding of an
                  EntityPack. const_cast was used so as to allow easy replacement of an
                  EntityPack without allowing replacement of the Entity itself.

        @return   Reference to the EntityPack being modified.
        *//*****************************************************************************/
        inline EntityPack& operator=(EntityPack& rhs)
        {
            const_cast<Pls::Entity&>(Entity) = rhs.Entity;
            tuple = rhs.tuple;
            return *this;
        }

      private:
        std::tuple<Components* ...> tuple;
    };

    /*---------------------------------------------------------------------------------*/
    /* EntityPack Template Functions Definitions                                       */
    /*---------------------------------------------------------------------------------*/
    template<typename ... Components>
    template<typename T>
    T& EntityPack<Components ...>::Get() const
    {
        static_assert((std::is_same_v<T, Components> || ...),
                      "Type parameter must be one of the type parameters of the EntityPack.");
        return *std::get<T*>(tuple);
    }

    template<typename ...Components>
    inline EntityPack<Components ...>::EntityPack(const Pls::Entity& entity, Components* ... components)
    : Entity{ entity }
    , tuple { std::make_tuple(components ...) }
    {}
}