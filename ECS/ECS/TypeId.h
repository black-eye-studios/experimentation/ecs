#pragma once

#include <type_traits>
#include <string>
#include <string_view>

#include "Plushie.h"
#include "Utility.h"

namespace Pls
{
    /********************************************************************************//*!
    @brief    Class for working with type hashes for identification of types at runtime.
    *//*********************************************************************************/
    class PLS_API TypeId
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Type Definitions                                                            */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Type of the variable that represents the hash of the type name.
        *//*****************************************************************************/
        using InternalHashType = decltype(Pls::HashCrc32(""));

        /*-----------------------------------------------------------------------------*/
        /* Static Functions                                                            */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Creates a unique TypeId that identifies the specified type.
              
        @tparam        T
                Type to get a hashed ID for. 
        @return   A TypeId object that uniquely identifies the specified type.
        *//*****************************************************************************/
        template<typename ... T>
        inline static constexpr TypeId Get() { return TypeId(Pls::HashCrc32(__FUNCSIG__)); }

        /*-----------------------------------------------------------------------------*/
        /* Getter Functions                                                            */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Creates a unique TypeId that identifies the specified type.

        @return   The integral hash value that is unique to this type.
        *//*****************************************************************************/
        inline constexpr InternalHashType GetId() const { return hash; }
        
        /*-----------------------------------------------------------------------------*/
        /* Overloaded Operators                                                        */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Checks if 2 TypeIds are the same.

        @params[in]    rhs
                The other TypeId to compare with.
        @return   True if the two types are the same.
        *//*****************************************************************************/
        bool operator==(const TypeId& rhs) const { return hash == rhs.hash; }
        /****************************************************************************//*!
        @brief    Checks if 2 TypeIds are different.

        @params[in]    rhs
                The other TypeId to compare with.
        @return   True if the two types are different.
        *//*****************************************************************************/
        bool operator!=(const TypeId& rhs) const { return hash != rhs.hash; }

      private:
        /*-----------------------------------------------------------------------------*/
        /* Constructors                                                                */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Creates a TypeId that encapsulates the internal id.

        @params[in]    value
                The value to set the internal hash to.
        *//*****************************************************************************/
        constexpr inline TypeId(InternalHashType value) : hash { value } {}

        /*-----------------------------------------------------------------------------*/
        /* Member Variables                                                            */
        /*-----------------------------------------------------------------------------*/
        InternalHashType hash;
    };
}