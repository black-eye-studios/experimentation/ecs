#pragma once

#include <vector>          // std::vector
#include <tuple>           // std::tuple
                           
#include "Entity.h"        // Pls::Entity
#include "EntityPack.h"    // Pls::EntityPack
#include "EntityManager.h" // Pls::EntityManager
#include "ContainerView.h" // Pls::ContainerView

namespace Pls
{
    /********************************************************************************//*!
    @brief    Abstract class that SceneView inherits from so that we can box/unbox 
              derived templated SceneViews.
    *//*********************************************************************************/
    class EntityPackListBase 
    { 
      public:
        /*-----------------------------------------------------------------------------*/
        /* Constructors/Destructors                                                    */
        /*-----------------------------------------------------------------------------*/
        virtual ~EntityPackListBase() = default;

        /*-----------------------------------------------------------------------------*/
        /* Getter Functions                                                            */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Retrieves the combination ID of this SceneView.

        @return   A TypeId that represents the ordered Type Combination for this 
                  SceneView object.
        *//*****************************************************************************/
        inline TypeId GetCombinationId() const { return componentComboId; }

      protected:
        /*-----------------------------------------------------------------------------*/
        /* Constructors/Destructors                                                    */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Constructs a SceneViewBase. This constructor is made protected to
                  prevent instantiation.

        @param[in]    comboType
                The TypeId that represents the ordered Type Combination for this 
                SceneView object.
        *//*****************************************************************************/
        inline EntityPackListBase(const TypeId& comboType) : componentComboId {comboType} {}

        /*-----------------------------------------------------------------------------*/
        /* Data Members                                                                */
        /*-----------------------------------------------------------------------------*/
        const TypeId componentComboId;
    };

    /********************************************************************************//*!
    @brief    Special iterator object that allows iteration through a list of objects
              that contain only specified components. Each object is represented as a
              EntityPack that provides access to the Entity and the Components owned by
              the Entity.
    *//*********************************************************************************/
    template<typename ... Components>
    class EntityPackList final : public EntityPackListBase
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Type Definitions                                                            */
        /*-----------------------------------------------------------------------------*/
        using SceneViewUnit  = EntityPack<Components ...>;
        using Container      = std::vector<SceneViewUnit>;
        using iterator       = typename Container::iterator;
        using const_iterator = typename Container::const_iterator;

        /*-----------------------------------------------------------------------------*/
        /* Constructors/Destructors                                                    */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Constructs a SceneView of Entities that contain the specified 
                  Components that are inside the specified ComponentStore.

        @param[in]    componentStore
                Read only reference to the ComponentStore that houses the Components 
                of Entities that this SceneView should feature.
        *//*****************************************************************************/
        explicit EntityPackList(const ComponentStore& componentStore);
        virtual ~EntityPackList() = default;
        
        /*-----------------------------------------------------------------------------*/
        /* Modification Functions                                                      */
        /*-----------------------------------------------------------------------------*/
        void insert(const Entity& entity, Components* ... components);
        void erase(const Entity& entity);

        /*-----------------------------------------------------------------------------*/
        /* Iterator Functions                                                          */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Retrieves an iterator pointing to the first EntityPack of the contiguious
                  array of EntityPacks stored by this SceneView. The order of EntityPacks
                  are guaranteed to be sequential in ther order of storage in the 
                  ComponentStore's SparseSets.

        @return   Iterator to the first EntityPack of the stored EntityPacks.
        *//*****************************************************************************/
        iterator       begin();
        /****************************************************************************//*!
        @brief    Retrieves a read only iterator pointing to the first EntityPack of the 
                  contiguious array of EntityPacks stored by this SceneView. The order of
                  EntityPacks are guaranteed to be sequential in ther order of storage in
                  the ComponentStore's SparseSets.

        @return   Iterator to the one-past-lebd EntityPack of the stored EntityPacks.
        *//*****************************************************************************/
        const_iterator begin() const;
        /****************************************************************************//*!
        @brief    Retrieves an iterator pointing to the one-past-end element of the 
                  contiguious array of EntityPacks stored by this SceneView.
                  
        @return   Iterator to the one-past-end EntityPack of the stored EntityPacks.
        *//*****************************************************************************/
        iterator       end();
        /****************************************************************************//*!
        @brief    Retrieves a read only iterator pointing to the one-past-end EntityPack
                  of the contiguious array of EntityPacks stored by this SceneView.
                  
        @return   Iterator to the one-past-end EntityPack of the stored EntityPacks.
        *//*****************************************************************************/
        const_iterator end() const;
        /****************************************************************************//*!
        @brief    Retrieves a read only iterator pointing to the first EntityPack of the 
                  contiguious array of EntityPacks stored by this SceneView. The order of
                  EntityPacks are guaranteed to be sequential in ther order of storage in
                  the ComponentStore's SparseSets.

        @return   Iterator to the one-past-lebd EntityPack of the stored EntityPacks.
        *//*****************************************************************************/
        const_iterator cbegin() const;
        /****************************************************************************//*!
        @brief    Retrieves a read only iterator pointing to the one-past-end EntityPack
                  of the contiguious array of EntityPacks stored by this SceneView.
                  
        @return   Iterator to the one-past-end EntityPack of the stored EntityPacks.
        *//*****************************************************************************/
        const_iterator cend()   const;

      private:
        Container entityPacks;
    };
    
    /********************************************************************************//*!
    @brief    Abstract class that SceneView inherits from so that we can box/unbox 
              derived templated SceneViews.
    *//*********************************************************************************/
    class SceneViewBase 
    { 
      public:
        /*-----------------------------------------------------------------------------*/
        /* Constructors/Destructors                                                    */
        /*-----------------------------------------------------------------------------*/
        virtual ~SceneViewBase() = default;
    };

    /********************************************************************************//*!
    @brief    Facade object for the EntityPackList that allows only the manipulation of
              EntityPacks in a referecned EntityPackList.
    *//*********************************************************************************/
    template<typename ... Components>
    class SceneView final : public SceneViewBase
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Type Definitions                                                            */
        /*-----------------------------------------------------------------------------*/
        using iterator       = typename EntityPackList<Components...>::iterator;
        using const_iterator = typename EntityPackList<Components...>::const_iterator;

        /*-----------------------------------------------------------------------------*/
        /* Constructors/Destructors                                                    */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Constructs a SceneView of a EntityPackList that is specified.

        @param[in]    entityPackList
                Reference to the EntityPackList to construct a facade for.
        *//*****************************************************************************/
        explicit SceneView(EntityPackList<Components ...>& entityPackList);
        virtual ~SceneView() = default;

        /*-----------------------------------------------------------------------------*/
        /* Iterator Functions                                                          */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Retrieves an iterator pointing to the first EntityPack of the contiguious
                  array of EntityPacks stored by this SceneView. The order of EntityPacks
                  are guaranteed to be sequential in ther order of storage in the 
                  ComponentStore's SparseSets.

        @return   Iterator to the first EntityPack of the stored EntityPacks.
        *//*****************************************************************************/
        inline iterator       begin();
        /****************************************************************************//*!
        @brief    Retrieves a read only iterator pointing to the first EntityPack of the 
                  contiguious array of EntityPacks stored by this SceneView. The order of
                  EntityPacks are guaranteed to be sequential in ther order of storage in
                  the ComponentStore's SparseSets.

        @return   Iterator to the one-past-lebd EntityPack of the stored EntityPacks.
        *//*****************************************************************************/
        inline const_iterator begin() const;
        /****************************************************************************//*!
        @brief    Retrieves an iterator pointing to the one-past-end element of the 
                  contiguious array of EntityPacks stored by this SceneView.
                  
        @return   Iterator to the one-past-end EntityPack of the stored EntityPacks.
        *//*****************************************************************************/
        inline iterator       end();
        /****************************************************************************//*!
        @brief    Retrieves a read only iterator pointing to the one-past-end EntityPack
                  of the contiguious array of EntityPacks stored by this SceneView.
                  
        @return   Iterator to the one-past-end EntityPack of the stored EntityPacks.
        *//*****************************************************************************/
        inline const_iterator end() const;
        /****************************************************************************//*!
        @brief    Retrieves a read only iterator pointing to the first EntityPack of the 
                  contiguious array of EntityPacks stored by this SceneView. The order of
                  EntityPacks are guaranteed to be sequential in ther order of storage in
                  the ComponentStore's SparseSets.

        @return   Iterator to the one-past-lebd EntityPack of the stored EntityPacks.
        *//*****************************************************************************/
        inline const_iterator cbegin() const;
        /****************************************************************************//*!
        @brief    Retrieves a read only iterator pointing to the one-past-end EntityPack
                  of the contiguious array of EntityPacks stored by this SceneView.
                  
        @return   Iterator to the one-past-end EntityPack of the stored EntityPacks.
        *//*****************************************************************************/
        inline const_iterator cend()   const;

      private:
        EntityPackList<Components ...>& entityPackList;
    };
    
    /********************************************************************************//*!
    @brief    Variant of EntityPackList that is essentially a wrapper to Entities.
    *//*********************************************************************************/
    //template<>
    //class EntityPackList<> : public EntityPackListBase
    //{
    //    /*-----------------------------------------------------------------------------*/
    //    /* Type Definitions                                                            */
    //    /*-----------------------------------------------------------------------------*/
    //    using SceneViewUnit  = EntityPack<>;
    //    using iterator       = typename EntityManager::iterator;
    //    using const_iterator = typename EntityManager::const_iterator;

    //    /*-----------------------------------------------------------------------------*/
    //    /* Constructors/Destructors                                                    */
    //    /*-----------------------------------------------------------------------------*/
    //    explicit SceneView(const EntityManager& entityManager);

    //    /*-----------------------------------------------------------------------------*/
    //    /* Iterator Functions                                                          */
    //    /*-----------------------------------------------------------------------------*/
    //    iterator       begin();
    //    iterator       end();
    //    const_iterator cbegin() const;
    //    const_iterator cend()   const;

    //  private:
    //    const EntityManager& entities;
    //};
    // TODO: Support EntityPackList<void>
    /*---------------------------------------------------------------------------------*/
    /* EntityPackList Template Functions Definitions                                   */
    /*---------------------------------------------------------------------------------*/
    template<typename ...Components>
    inline EntityPackList<Components...>::EntityPackList(const ComponentStore& componentStore)
    : EntityPackListBase{ TypeId::Get<Components ...>() }
    {
        // Get the smallest entity list
        struct TypeAggregateData
        {
            TypeId TypeId;
            size_t ComponentCount;
            Range<ComponentStore::dense_const_iterator> Entities;
        };
        std::vector<TypeAggregateData> componentAggregates = 
        { 
            TypeAggregateData{TypeId::Get<Components>(),
                              componentStore.GetCount<Components>(),
                              componentStore.GetEntities<Components>()}
            ... 
        };
        TypeAggregateData* smallest = &componentAggregates[0];
        for (auto& componentSet : componentAggregates)
        {
            if (smallest->ComponentCount > componentSet.ComponentCount)
            {
                smallest = &componentSet;
            }
        }

        // Iterate through the smallest component set if it is not empty
        if (!smallest->Entities.empty())
        {
            for (const auto& entityId : smallest->Entities)
            {
                const Entity ENTITY = Entity(entityId);
                if ((componentStore.Has<Components>(ENTITY) && ...))
                {
                    // Create entity pack and put it into the SceneView
                    entityPacks.emplace_back(ENTITY, componentStore.Get<Components>(ENTITY) ...);
                }
            }
        }
    }
    template<typename ...Components>
    inline void EntityPackList<Components ...>::insert(const Entity& entity, Components * ...components)
    {
        // HACK: For now, we can simply place it at the back due to how the SparseSet works
        // TODO: This is still not ideal as multiple insertions will cause fragmentation
        //       for multi component SceneViews.
        entityPacks.emplace_back(entity, components ...);
    }
    template<typename ...Components>
    inline void EntityPackList<Components ...>::erase(const Entity& entity)
    {
        // HACK: For now, we can simply swap the back and removee just like how SparseSet removal works
        // TODO: This is still not ideal as multiple removals will cause fragmentation
        //       for multi component SceneViews.
        
        // Look for the entity in the SceneView
        auto removedItem = entityPacks.end();
        for (auto iter = entityPacks.begin(); iter != entityPacks.end(); ++iter)
        {
            if (iter->Entity == entity)
            {
                removedItem = iter;
                break;
            }
        }

        // Found the element to remove, swap and pop like the Sparse Set
        *removedItem = *(entityPacks.end() - 1);
        entityPacks.pop_back();
    }
    template<typename ...Components>
    inline typename EntityPackList<Components ...>::iterator EntityPackList<Components ...>::begin()
    {
        return entityPacks.begin();
    }
    template<typename ...Components>
    inline typename EntityPackList<Components ...>::const_iterator EntityPackList<Components ...>::begin() const
    {
        return entityPacks.begin();
    }
    template<typename ...Components>
    inline typename EntityPackList<Components ...>::iterator EntityPackList<Components ...>::end()
    {
        return entityPacks.end();
    }
    template<typename ...Components>
    inline typename EntityPackList<Components ...>::const_iterator EntityPackList<Components ...>::end() const
    {
        return entityPacks.end();
    }
    template<typename ...Components>
    inline typename EntityPackList<Components ...>::const_iterator EntityPackList<Components ...>::cbegin() const
    {
        return entityPacks.cbegin();
    }
    template<typename ...Components>
    inline typename EntityPackList<Components ...>::const_iterator EntityPackList<Components ...>::cend() const
    {
        return entityPacks.cend();
    }
    /*---------------------------------------------------------------------------------*/
    /* SceneView Template Functions Definitions                                        */
    /*---------------------------------------------------------------------------------*/
    template<typename ...Components>
    inline SceneView<Components...>::SceneView(EntityPackList<Components...>& entityPackList)
    : entityPackList{ entityPackList }
    {}
    template<typename ...Components>
    inline typename SceneView<Components ...>::iterator SceneView<Components ...>::begin()
    {
        return entityPackList.begin();
    }
    template<typename ...Components>
    inline typename SceneView<Components ...>::const_iterator SceneView<Components ...>::begin() const
    {
        return entityPackList.begin();
    }
    template<typename ...Components>
    inline typename SceneView<Components ...>::iterator SceneView<Components ...>::end()
    {
        return entityPackList.end();
    }
    template<typename ...Components>
    inline typename SceneView<Components ...>::const_iterator SceneView<Components ...>::end() const
    {
        return entityPackList.end();
    }
    template<typename ...Components>
    inline typename SceneView<Components ...>::const_iterator SceneView<Components ...>::cbegin() const
    {
        return entityPackList.cbegin();
    }
    template<typename ...Components>
    inline typename SceneView<Components ...>::const_iterator SceneView<Components ...>::cend() const
    {
        return entityPackList.cend();
    }
}