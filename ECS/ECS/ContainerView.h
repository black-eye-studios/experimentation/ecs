#pragma once

namespace Pls
{
    /********************************************************************************//*!
    @brief    Object that allows iteration through any Container-like object that
              implements iterator functions: begin(), end(), cbegin(), cend().
    *//*********************************************************************************/
    template<typename T>
    class ContainerView final
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Type Definitions                                                            */
        /*-----------------------------------------------------------------------------*/
        using iterator       = typename T::iterator;
        using const_iterator = typename T::const_iterator;
        
        /*-----------------------------------------------------------------------------*/
        /* Constructors/Destructors                                                    */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Constructs a ContainerView of the specified Container-like object.

        @param[in]    container
                Read only reference to the Container to create a ContainerView for.
        *//*****************************************************************************/
        ContainerView(const T& container);
        ~ContainerView() = default;
        
        /*-----------------------------------------------------------------------------*/
        /* Iterator Functions                                                          */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Retrieves an iterator pointing to the first element of the referenced
                  Container-like object.

        @return   Iterator to the first element of the referenced Container-like object.
        *//*****************************************************************************/
        iterator       begin();
        /****************************************************************************//*!
        @brief    Retrieves an iterator pointing to the one-past-end element of the 
                  referenced Container-like object.

        @return   Iterator to the one-past-end element of the referenced Container-like 
                  object.
        *//*****************************************************************************/
        iterator       end();
        /****************************************************************************//*!
        @brief    Retrieves a read only iterator pointing to the first element of the
                  referenced Container-like object.

        @return   Read only iterator to the first element of the referenced 
                  Container-like object.
        *//*****************************************************************************/
        const_iterator cbegin() const;
        /****************************************************************************//*!
        @brief    Retrieves a read only iterator pointing to the one-past-end element of
                  the referenced Container-like object.

        @return   Read only iterator to the one-past-end element of the referenced 
                  Container-like object.
        *//*****************************************************************************/
        const_iterator cend()   const;

      protected:
        /*-----------------------------------------------------------------------------*/
        /* Data Members                                                                */
        /*-----------------------------------------------------------------------------*/
        const T& container;
    };

    /*---------------------------------------------------------------------------------*/
    /* ContainerView Template Functions Definitions - Constructor                      */
    /*---------------------------------------------------------------------------------*/
    template<typename T>
    inline ContainerView<T>::ContainerView(const T& container)
    : container { container }
    {}

    /*---------------------------------------------------------------------------------*/
    /* ContainerView Template Functions Definitions - Iterator Functions               */
    /*---------------------------------------------------------------------------------*/
    template<typename T>
    inline typename ContainerView<T>::iterator ContainerView<T>::begin()
    {
        return container.begin();
    }
    template<typename T>
    inline typename ContainerView<T>::iterator ContainerView<T>::end()
    {
        return container.end();
    }
    template<typename T>
    inline typename ContainerView<T>::const_iterator ContainerView<T>::cbegin() const
    {
        return container.cbegin();
    }
    template<typename T>
    inline typename ContainerView<T>::const_iterator ContainerView<T>::cend() const
    {
        return container.cend();
    }
    
}