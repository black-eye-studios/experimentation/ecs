#pragma once

#include <cstdint>   // uint32_t
#include <array>     // std::array
#include <limits>    // std::numeric_limits
#include <stdexcept> // std::out_of_range, std::invalid_argument
#include <memory>    // std::addressof
#include <algorithm> // std::sort

namespace Pls
{
    /********************************************************************************//*!
    @brief    Struct of static members that contains constant definitions used by the 
              SparseSet implementations.
    *//*********************************************************************************/
    struct SparseSetDefines
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Constants                                                                   */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    The default size used for a SparseSet.
        *//*****************************************************************************/
        static constexpr size_t DEFAULT_SIZE = 256;
    };

    /********************************************************************************//*!
    @brief    Simple class that SparseSet is to inherit from. This is present to allow
              pointers or references to generic SparseSets and to invoke functions that
              are not performance critical and would benefit from being exposed in a 
              generic manner despite incurring a virtual dispatch cost.        
    @tparam        SZ
            Size of the SparseSet. This is the maximum number of elements that can be
            stored by the SparseSet.
    *//*********************************************************************************/
    template<size_t SZ = SparseSetDefines::DEFAULT_SIZE>
    class SparseSetBase
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Constants                                                                   */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Maximum size of the SparseSet.
        *//*****************************************************************************/
        static constexpr size_t SIZE = SZ;
        /*-----------------------------------------------------------------------------*/
        /* Type Definitions                                                            */
        /*-----------------------------------------------------------------------------*/
        using index_type           = uint16_t;
        using size_type            = size_t;
        // Inverse Dense Iterator Types
        using dense_const_iterator = typename std::array<index_type, SIZE>::const_iterator;
        
        /*-----------------------------------------------------------------------------*/
        /* Destructors                                                                 */
        /*-----------------------------------------------------------------------------*/
        virtual ~SparseSetBase() = default;

        /*-----------------------------------------------------------------------------*/
        /* Usage Functions                                                             */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Removes an element at the specified index.

        @throws        std::invalid_argument
                If an element does not exist in the set.

        @params[in]    idx
                Index to remove an element at.
        *//*****************************************************************************/
        virtual void erase(index_type idx) = 0;        
        /****************************************************************************//*!
        @brief    Removes all elements from the set.
        *//*****************************************************************************/
        virtual void clear() = 0;

        /*-----------------------------------------------------------------------------*/
        /* Inverse Array Iterator Functions                                            */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Retrieves a read only iterator pointing to the first element of the 
                  contiguious array of elements stored by this set. The order of elements
                  do not necessary follow their insertion order unless no removals have 
                  taken place.

        @return   Read only iterator to the first element of the stored elements.
        *//*****************************************************************************/
        inline dense_const_iterator dense_cbegin() const 
        {
            return inverseSparseArray.cbegin(); 
        }
        /****************************************************************************//*!
        @brief    Retrieves a read only iterator pointing to the one-past-end element of 
                  the contiguious array of elements stored by this set. 
                  When used in a for loop, it is advised to cache this value for better
                  performance. Alternatively, use std::for_each which caches the iterator
                  passed in as cend().

        @return   Read only iterator to the first element of the stored elements.
        *//*****************************************************************************/
        inline dense_const_iterator dense_cend() const 
        { 
            return inverseSparseArray.cbegin() + SparseSetBase<SZ>::back; 
        }

      protected:
        /*-----------------------------------------------------------------------------*/
        /* Protected Constructor (No Instantiation)                                    */
        /*-----------------------------------------------------------------------------*/
        inline SparseSetBase();

        /*-----------------------------------------------------------------------------*/
        /* Constants                                                                   */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Maximum index in the sparse set.
        *//*****************************************************************************/
        static constexpr index_type MAX_INDEX = SIZE - 1;
        /****************************************************************************//*!
        @brief    Value used to mark an element in the sparse and inverse sparse arrays
                  as unused.
        *//*****************************************************************************/
        static constexpr index_type INVALID = std::numeric_limits<index_type>().max();

        /*-----------------------------------------------------------------------------*/
        /* Data Members                                                                */
        /*-----------------------------------------------------------------------------*/
        std::array<index_type, SIZE> inverseSparseArray; // Maps dense-index -> sparse-index
        std::array<index_type, SIZE> sparseArray;        // Maps sparse-index -> dense-index
        index_type                   back;               // Stores the index of one past last in the dense array

    };

    /********************************************************************************//*!
    @brief    Defines a generic template Sparse Set that provides O(1) access, removal
              and insertion at the cost of memory usage.

              This class's functions naming conventions will not follow standard
              conventions of the project in order to match the API of STL containers for
              std::algorithm compatibility.
              
    @tparam        T
            Type of elements stored by the SparseSet.              
    @tparam        SZ
            Size of the SparseSet. This is the maximum number of elements that can be
            stored by the SparseSet.
    *//*********************************************************************************/
    template<typename T, size_t SZ = SparseSetDefines::DEFAULT_SIZE>
    class SparseSet final : public SparseSetBase<SZ>
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Type Definitions                                                            */
        /*-----------------------------------------------------------------------------*/
        using size_type       = typename SparseSetBase<SZ>::size_type;
        using index_type      = typename SparseSetBase<SZ>::index_type;
        // Container's Types
        using value_type      = T;
        using pointer         = T*;
        using const_pointer   = const T*;
        using reference       = T&;
        using const_reference = const T&;
        // Iterator Types
        using iterator = typename std::array<T, SZ>::iterator;
        using const_iterator = typename std::array<T, SZ>::const_iterator;
        using reverse_iterator = typename std::array<T, SZ>::reverse_iterator;
        using const_reverse_iterator = typename std::array<T, SZ>::const_reverse_iterator;

        /*-----------------------------------------------------------------------------*/
        /* Constants                                                                   */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    The maximum number of elements that can be stored by this SparseSet.
        *//*****************************************************************************/
        static constexpr size_type CAPACITY = SZ;

        /*-----------------------------------------------------------------------------*/
        /* Constructors/Destructors                                                    */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Default constructor.
        *//*****************************************************************************/
        SparseSet();
        virtual ~SparseSet() = default;

        // Disallow moving or copying
        SparseSet(const SparseSet&) = delete;
        SparseSet(SparseSet&&) = delete;

        /*-----------------------------------------------------------------------------*/
        /* Usage Functions                                                             */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Inserts an element at the specified index.
        
        @throws        std::invalid_argument
                If an element already exists in the set.
        @throws        std::out_of_range
                If an index beyond the capacity of this SparseSet was specified.

        @params[in]    idx
                Index to add an element at.

        @return   Reference to the inserted element.
        *//*****************************************************************************/
        reference       insert(index_type idx);
        /****************************************************************************//*!
        @brief    Removes an element at the specified index.

        @throws        std::invalid_argument
                If an element does not exist in the set.
        @throws        std::out_of_range
                If an index beyond the capacity of this SparseSet was specified.

        @params[in]    idx
                Index to remove an element at.
        *//*****************************************************************************/
        void            erase(index_type idx) override;
        /****************************************************************************//*!
        @brief    Retrieves an element at the specified index.

        @throws        std::out_of_range
                If an element does not exist in the set or index specified is beyond the
                capacity of the set.

        @params[in]    idx
                Index of the element to retrieve.

        @return   Reference to the inserted element.
        *//*****************************************************************************/
        reference       at(index_type idx);        
        /****************************************************************************//*!
        @brief    Retrieves an element at the specified index.

        @throws        std::out_of_range
                If an element does not exist in the set or index specified is beyond the
                capacity of the set.

        @params[in]    idx
                Index of the element to retrieve.

        @return   Read only reference to the inserted element.
        *//*****************************************************************************/
        const_reference at(index_type idx) const;
        /****************************************************************************//*!
        @brief    Checks if an element of the specified index exists.

        @params[in]    idx
                Index to check if element at such an index exists.

        @return   True if the element exists in the set.
        *//*****************************************************************************/
        bool            contains(index_type idx) const;
        /****************************************************************************//*!
        @brief    Retrieves an iterator to an element at the specified index if it 
                  exists.

        @params[in]    idx
                Index of the element to find.

        @return   Iterator pointing to the element to find. If none is found, end() is
                  returned.
        *//*****************************************************************************/
        iterator        find(index_type idx);
        /****************************************************************************//*!
        @brief    Retrieves a const_iterator to an element at the specified index if it 
                  exists.

        @params[in]    idx
                Index of the element to find.

        @return   Const_iterator pointing to the element to find. If none is found, 
                  cend() is returned.
        *//*****************************************************************************/
        const_iterator  find(index_type idx) const;
        /****************************************************************************//*!
        @brief    Retrieves the number of elements in the set. This does not include
                  invalid elements.

        @return   Number of elements added to the set since construction.
        *//*****************************************************************************/
        size_type       size() const;
        /****************************************************************************//*!
        @brief    Retrieves the maximum number of elements that can be stored by the set.
                  This is equal to CAPACITY.

        @return   Maximum number of elements that can be stored by the set.
        *//*****************************************************************************/
        size_type       capacity() const;
        /****************************************************************************//*!
        @brief    Whether or not there are any elements in the set.

        @return   True if there is at least one element in the set.
        *//*****************************************************************************/
        bool            empty() const;
        
        /****************************************************************************//*!
        @brief    Removes all elements from the set.
        *//*****************************************************************************/
        void            clear() override;
        /****************************************************************************//*!
        @brief    Rearranges the elements stored in the dense array of the SparseSet in
                  the specified order. Note that this is a potentially expensive 
                  operation.

                  Error checking of the passed in ordered indices is done in debug 
                  builds but this is omitted in release builds for performance reasons.
                  
        @throws        std::invalid_argument
                If the vector contains duplicate values and there are indices that are 
                not accounted for in debug builds.

        @params[in]    orderedIndices
                Read only reference to a vector that contains the sparse indices that
                the dense array should be ordered in. This vector cannot contain 
                duplicate indices and all currently valid sparse indices must be present.
        *//*****************************************************************************/
        void            order(const std::vector<index_type>& orderedIndices);

        /*-----------------------------------------------------------------------------*/
        /* Iterator Functions                                                          */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Retrieves an iterator pointing to the first element of the contiguious
                  array of elements stored by this set. The order of elements do not
                  necessary follow their insertion order unless no removals have taken
                  place.

                  However, if order() was used before this function call with no insert()
                  or erase() in between, the order of the elements i guaranteed to match 
                  the indices passed by order().

        @return   Iterator to the first element of the stored elements.
        *//*****************************************************************************/
        iterator begin();
        /****************************************************************************//*!
        @brief    Retrieves an iterator pointing to the one-past-end element of the 
                  contiguious array of elements stored by this set.
                  When used in a for loop, it is advised to cache this value for better
                  performance. Alternatively, use std::for_each which caches the iterator
                  passed in as end().
                  
        @return   Iterator to the one-past-end element of the stored elements.
        *//*****************************************************************************/
        iterator end();
        /****************************************************************************//*!
        @brief    Retrieves a read only iterator pointing to the first element of the 
                  contiguious array of elements stored by this set. The order of elements
                  do not necessary follow their insertion order unless no removals have 
                  taken place.

        @return   Read only iterator to the first element of the stored elements.
        *//*****************************************************************************/
        const_iterator cbegin() const;
        /****************************************************************************//*!
        @brief    Retrieves a read only iterator pointing to the one-past-end element of 
                  the contiguious array of elements stored by this set. 
                  When used in a for loop, it is advised to cache this value for better
                  performance. Alternatively, use std::for_each which caches the iterator
                  passed in as cend().

        @return   Read only iterator to the first element of the stored elements.
        *//*****************************************************************************/
        const_iterator cend() const;
        /****************************************************************************//*!
        @brief    Retrieves a reverse iterator pointing to the last element of the 
                  contiguious array of elements stored by this set. The order of elements
                  do not necessary follow their insertion order unless no removals have 
                  taken place.

        @return   Reverse iterator to the last element of the stored elements.
        *//*****************************************************************************/
        reverse_iterator rbegin();
        /****************************************************************************//*!
        @brief    Retrieves an reverse iterator pointing to the one-behind-first element 
                  of the contiguious array of elements stored by this set. 
                  When used in a for loop, it is advised to cache this value for better
                  performance. Alternatively, use std::for_each which caches the iterator
                  passed in as rend().
                  
        @return   Reverse iterator to the one-behind-first element of the stored elements.
        *//*****************************************************************************/
        reverse_iterator rend();
        /****************************************************************************//*!
        @brief    Retrieves a read only reverse iterator pointing to the last element of 
                  the contiguious array of elements stored by this set. The order of 
                  elements do not necessary follow their insertion order unless no 
                  removals have taken place.

        @return   Read only reverse iterator to the last element of the stored elements.
        *//*****************************************************************************/
        const_reverse_iterator crbegin() const;
        /****************************************************************************//*!
        @brief    Retrieves a read only reverse iterator pointing to the one-behind-first
                  element of the contiguious array of elements stored by this set. 
                  
        @return   Read only reverse iterator to the one-behind-first element of the 
                  stored elements.
        *//*****************************************************************************/
        const_reverse_iterator crend() const;

        /*-----------------------------------------------------------------------------*/
        /* Convenience Operator Functions                                              */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Retrieves an element at the specified index.

        @throws        std::invalid_argument
                If an element does not exist in the set.

        @params[in]    idx
                Index of the element to retrieve.

        @return   Reference to the inserted element.
        *//*****************************************************************************/
        reference operator[](index_type idx);
        /****************************************************************************//*!
        @brief    Retrieves an element at the specified index.

        @throws        std::invalid_argument
                If an element does not exist in the set.

        @params[in]    idx
                Index of the element to retrieve.

        @return   Read only reference to the inserted element.
        *//*****************************************************************************/
        const_reference operator[](index_type idx) const;

      private:
        /*-----------------------------------------------------------------------------*/
        /* Data Members                                                                */
        /*-----------------------------------------------------------------------------*/
        std::array<T, SZ>          denseArray;
    };
    
    /*---------------------------------------------------------------------------------*/
    /* Constructor                                                                     */
    /*---------------------------------------------------------------------------------*/
    template<size_t SZ>
    SparseSetBase<SZ>::SparseSetBase()
    : sparseArray {INVALID}
    , inverseSparseArray {INVALID}
    , back {0}
    {
        // Limit the size of the SparseSet
        static_assert(SZ < INVALID, "The maximum size of each SparseSet cannot be greater or equal to index_type max value (65536).");

        // Invalidate the sparse array
        sparseArray.fill(INVALID);
        inverseSparseArray.fill(INVALID);
    }
    template<typename T, size_t SZ>
    SparseSet<T, SZ>::SparseSet()
    : SparseSetBase<SZ>{}
    {        
        static_assert(std::is_move_assignable<T>::value, "Objects stored by the SparseSet must be move-assignable.");
        
        // Default initialize
        denseArray.fill(T());
    }
    /*---------------------------------------------------------------------------------*/
    /* Usage Functions                                                                 */
    /*---------------------------------------------------------------------------------*/
    template<typename T, size_t SZ>
    inline typename SparseSet<T, SZ>::reference SparseSet<T, SZ>::insert(index_type idx)
    {
        if (idx > SparseSetBase<SZ>::MAX_INDEX) throw std::out_of_range("An out of range index was specified!");
        if (contains(idx)) throw std::invalid_argument("An element at this index already exists!");
        const index_type DENSE_IDX = SparseSetBase<SZ>::back++;
        SparseSetBase<SZ>::sparseArray[idx] = DENSE_IDX;
        SparseSetBase<SZ>::inverseSparseArray[DENSE_IDX] = idx;

        return denseArray[DENSE_IDX];
    }
    template<typename T, size_t SZ>
    inline void SparseSet<T, SZ>::erase(index_type idx)
    {
        if (idx > SparseSetBase<SZ>::MAX_INDEX) throw std::out_of_range("An out of range index was specified!");
        if (!contains(idx)) throw std::invalid_argument("An element at this index does not exist!");

        // Shift the one past last back
        --SparseSetBase<SZ>::back;
        // Swap with the last element
        std::swap(denseArray[SparseSetBase<SZ>::sparseArray[idx]], denseArray[SparseSetBase<SZ>::back]);
        // Update the sparse set by swapping the indices
        SparseSetBase<SZ>::sparseArray[SparseSetBase<SZ>::inverseSparseArray[SparseSetBase<SZ>::back]] = SparseSetBase<SZ>::sparseArray[idx];
        SparseSetBase<SZ>::inverseSparseArray[SparseSetBase<SZ>::sparseArray[idx]] = SparseSetBase<SZ>::inverseSparseArray[SparseSetBase<SZ>::back];
        // Mark the "removed" values as invalid
        SparseSetBase<SZ>::sparseArray[idx] = SparseSetBase<SZ>::INVALID;
        SparseSetBase<SZ>::inverseSparseArray[SparseSetBase<SZ>::back] = SparseSetBase<SZ>::INVALID;
        // Reset the dense array value
        denseArray[SparseSetBase<SZ>::back] = T();
    }
    template<typename T, size_t SZ>
    inline typename SparseSet<T, SZ>::reference SparseSet<T, SZ>::at(index_type idx)
    {
        return const_cast<reference>(static_cast<const SparseSet<T, SZ>&>(*this).at(idx));
    }
    template<typename T, size_t SZ>
    inline typename SparseSet<T, SZ>::const_reference SparseSet<T, SZ>::at(index_type idx) const
    {
        // Range Check
        if (idx > SparseSetBase<SZ>::MAX_INDEX) throw std::out_of_range("An out of range index was specified!");
        if (!contains(idx)) throw std::out_of_range("An element at this index does not exist!");
        return denseArray[SparseSetBase<SZ>::sparseArray[idx]];
    }
    template<typename T, size_t SZ>
    inline bool SparseSet<T, SZ>::contains(index_type idx) const
    {
        if (idx > SparseSetBase<SZ>::MAX_INDEX) throw std::out_of_range("Index is out of range!");
        return SparseSetBase<SZ>::sparseArray[idx] != SparseSetBase<SZ>::INVALID;
    }
    template<typename T, size_t SZ>
    inline typename SparseSet<T, SZ>::iterator SparseSet<T, SZ>::find(index_type idx)
    {
        if (idx > SparseSetBase<SZ>::MAX_INDEX || SparseSetBase<SZ>::sparseArray[idx] == SparseSetBase<SZ>::INVALID)
        {
            return end();
        }
        return begin() + idx;
    }
    template<typename T, size_t SZ>
    inline typename SparseSet<T, SZ>::const_iterator SparseSet<T, SZ>::find(index_type idx) const
    {
        if (idx > SparseSetBase<SZ>::MAX_INDEX || SparseSetBase<SZ>::sparseArray[idx] == SparseSetBase<SZ>::INVALID)
        {
            return cend();
        }
        return cbegin() + idx;
    }
    template<typename T, size_t SZ>
    inline typename SparseSet<T, SZ>::size_type SparseSet<T, SZ>::size() const
    {
        return SparseSetBase<SZ>::back;
    }
    template<typename T, size_t SZ>
    inline typename SparseSet<T, SZ>::size_type SparseSet<T, SZ>::capacity() const
    {
        return CAPACITY;
    }
    template<typename T, size_t SZ>
    inline bool SparseSet<T, SZ>::empty() const
    {
        return SparseSetBase<SZ>::back == 0;
    }
    template<typename T, size_t SZ>
    inline void SparseSet<T, SZ>::clear()
    {
        // Default initialize
        denseArray.fill(T());

        // Invalidate the sparse array
        SparseSetBase<SZ>::sparseArray.fill(SparseSetBase<SZ>::INVALID);
        SparseSetBase<SZ>::inverseSparseArray.fill(SparseSetBase<SZ>::INVALID);

        // Reset back tracker
        SparseSetBase<SZ>::back = 0;
    }
    template<typename T, size_t SZ>
    inline void SparseSet<T, SZ>::order(const std::vector<index_type>& orderedIndices)
    {
#if _DEBUG
        // Check orderedIndices for errors
        // -- Ensure Unique
        std::vector<index_type> sortedIndices{ orderedIndices };
        std::sort(sortedIndices.begin(), sortedIndices.end());
        if (std::adjacent_find(sortedIndices.cbegin(), sortedIndices.cend()) != sortedIndices.cend())
        {
            throw std::invalid_argument("[SparseSet] Duplicate indices found while overriding order!");
        }
        // -- Ensure all valid indices are accounted for
        std::vector<index_type> validIndices;
        for (index_type i = 0; i < SparseSetBase<SZ>::inverseSparseArray.size() && SparseSetBase<SZ>::inverseSparseArray[i] != SparseSetBase<SZ>::INVALID; ++i)
        {
            validIndices.emplace_back(SparseSetBase<SZ>::inverseSparseArray[i]);
        }
        if (validIndices.size() != this->size())
        {
            throw std::invalid_argument("[SparseSet] Insufficient indices for overriding order!");
        }
        std::sort(validIndices.begin(), validIndices.end());
        for (index_type i = 0; i < validIndices.size(); ++i)
        {
            if (validIndices[i] != sortedIndices[i])
            {
                throw std::invalid_argument("[SparseSet] Missing indices for overriding order!");
            }
        }
#endif

        // Rearrange elements in the dense array
        const std::vector<T> ORIGINAL_DATA { begin(), end() };
        for (index_type i = 0; i < orderedIndices.size(); ++i)
        {
            // Position the dense element in the reordered spot
            denseArray[i] = ORIGINAL_DATA[SparseSetBase<SZ>::sparseArray[orderedIndices[i]]];

            // Update sparse and inverse sparse array
            SparseSetBase<SZ>::sparseArray[orderedIndices[i]] = i;
            SparseSetBase<SZ>::inverseSparseArray[i] = orderedIndices[i];
        }
    }
    /*---------------------------------------------------------------------------------*/
    /* Iterator Functions                                                              */
    /*---------------------------------------------------------------------------------*/
    template<typename T, size_t SZ>
    inline typename SparseSet<T, SZ>::iterator SparseSet<T, SZ>::begin()
    {
        return denseArray.begin();
    }
    template<typename T, size_t SZ>
    inline typename SparseSet<T, SZ>::iterator SparseSet<T, SZ>::end()
    {
        return denseArray.begin() + SparseSetBase<SZ>::back;
    }
    template<typename T, size_t SZ>
    inline typename SparseSet<T, SZ>::const_iterator SparseSet<T, SZ>::cbegin() const
    {
        return denseArray.cbegin();
    }
    template<typename T, size_t SZ>
    inline typename SparseSet<T, SZ>::const_iterator SparseSet<T, SZ>::cend() const
    {
        return denseArray.cbegin() + SparseSetBase<SZ>::back;
    }
    template<typename T, size_t SZ>
    inline typename SparseSet<T, SZ>::reverse_iterator SparseSet<T, SZ>::rbegin()
    {
        return denseArray.rend() - SparseSetBase<SZ>::back;
    }
    template<typename T, size_t SZ>
    inline typename SparseSet<T, SZ>::reverse_iterator SparseSet<T, SZ>::rend()
    {
        return denseArray.rend();
    }
    template<typename T, size_t SZ>
    inline typename SparseSet<T, SZ>::const_reverse_iterator SparseSet<T, SZ>::crbegin() const
    {
        return denseArray.crend() - SparseSetBase<SZ>::back;
    }
    template<typename T, size_t SZ>
    inline typename SparseSet<T, SZ>::const_reverse_iterator SparseSet<T, SZ>::crend() const
    {
        return denseArray.crend();
    }
    /*---------------------------------------------------------------------------------*/
    /* Convenience Operator Functions                                                  */
    /*---------------------------------------------------------------------------------*/
    template<typename T, size_t SZ>
    inline typename SparseSet<T, SZ>::reference SparseSet<T, SZ>::operator[](index_type idx)
    {
        return at(idx);
    }
    template<typename T, size_t SZ>
    inline typename SparseSet<T, SZ>::const_reference SparseSet<T, SZ>::operator[](index_type idx) const
    {
        return at(idx);
    }
}

