#pragma once

#include <queue> // std::queue

#include "Entity.h" // Pls::Entity

namespace Pls
{
    /********************************************************************************//*!
    @brief    Class for managing Entities and recycling the IDs for usage.
              
              Current implementation is not optimized and Destroy() works in O(n) time.
    *//*********************************************************************************/
    class EntityManager final
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Type Definitions                                                            */
        /*-----------------------------------------------------------------------------*/
        using container = std::vector<Entity>;
        using iterator = std::vector<Entity>::iterator;
        using const_iterator = std::vector<Entity>::const_iterator;

        /*-----------------------------------------------------------------------------*/
        /* Usage Functions                                                             */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Creates a Entity and provides the Entity object for ECS-related
                  operations.

        @return   An Entity object that represents the created Entity.
        *//*****************************************************************************/
        Entity Create();
        /****************************************************************************//*!
        @brief    Destroys the specified Entity. This Entity is actually recycled.
                  Currently, this is not optimized and it works in O(n) time.

        @param[in]    entity
                The Entity object that should be destroyed.
        *//*****************************************************************************/
        void   Destroy(const Entity& entity);
        /****************************************************************************//*!
        @brief    Clears the Entity lists and sets everything back to their defaults.
        *//*****************************************************************************/
        void   Clear();
        
        /*-----------------------------------------------------------------------------*/
        /* Iterator Functions                                                          */
        /*-----------------------------------------------------------------------------*/
      private:
        Entity::IDType back = 0;
        container entities;
        std::queue<Entity> freeList;

        /*-----------------------------------------------------------------------------*/
        /* Helper Functions                                                            */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Helper function to get an available Entry from the freeList and pops
                  it from the freeList. If none are available, a new one is generated.
                  This function is present as Entities cannot be normally created without
                  any parameters.

        @return   An Entity object that represents the created Entity.
        *//*****************************************************************************/
        Entity getAvailableFreeEntity();
    };
}

