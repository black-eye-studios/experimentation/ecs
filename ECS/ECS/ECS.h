#pragma once

#include <vector>           // std::vector
#include <unordered_map>    // std::unordered_map

#include "EntityManager.h"  // Pls::EntityManager
#include "ComponentStore.h" // Pls::ComponentStore
#include "System.h"         // Pls::System
#include "ContainerView.h"  // Pls::ContainerView
#include "Range.h"          // Pls::Range
#include "SceneView.h"      // Pls::SceneView
#include "Event.h"          // Pls::Event

namespace Pls
{
    /********************************************************************************//*!
    @brief    Static ECS class that encapsulates the Entity Component System as a static
              class.
    *//*********************************************************************************/
    class ECS final
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Type Definitions                                                            */
        /*-----------------------------------------------------------------------------*/
        using EntitiesView   = ContainerView<EntityManager>;
        using EntityEvent = Event<Entity>;

        /*-----------------------------------------------------------------------------*/
        /* Public Data Members                                                         */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Use this variable to iterate through available Entities. This supports
                  iterators so you may use a range based for loop for iteration.
        *//*****************************************************************************/         
        static const EntitiesView Entities;

        /*-----------------------------------------------------------------------------*/
        /* Constructors/Destructors                                                    */
        /*-----------------------------------------------------------------------------*/
        // Default Constructors deleted to prevent instantiation of static class
        ECS()  = delete;
        ~ECS() = delete;

        /*-----------------------------------------------------------------------------*/
        /* Lifecycle Functions                                                         */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Executes the SystemRoutines in the order they were added to update all
                  Components tied to valid Entities in the ECS.
        *//*****************************************************************************/
        static void UpdateSystems();
        /****************************************************************************//*!
        @brief    Resets all properties of the ECS, all Entities, Components and Systems
                  will be dumped and/or reset to their initial state.
        *//*****************************************************************************/
        static void Reset();

        /*-----------------------------------------------------------------------------*/
        /* Usage Functions - Entity                                                    */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Creates an Entity in the ECS and returns the created Entity.

        @return   Entity object that represents the Entity that was created.
        *//*****************************************************************************/
        static Entity CreateEntity();
        /****************************************************************************//*!
        @brief    Creates an Entity in the ECS with the specified Components added 
                  and returns the created Entity and Components as an EntityPack.

        @tparam       Components
                Argument pack of Components that should be added to the created Entity.

        @return   EntityPack that contains the created Entity and pointers to the added
                  Components.
        *//*****************************************************************************/
        template<typename ... Components>
        static EntityPack<Components ...> CreateEntity();
        /****************************************************************************//*!
        @brief    Destroys a specified Entity and removes it from usage.

        @param[in]    entity
                Read only reference to an Entity that represents the Entity to destroy.
        *//*****************************************************************************/
        static void   DestroyEntity(const Entity& entity);
        
        /*-----------------------------------------------------------------------------*/
        /* Usage Functions - Components                                                */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Adds a Component to the specified Entity.

        @throws    std::invalid_argument
                If the Component already exists.
                
        @tparam       Component
                Type of Component to add to the specified Entity.
        @param[in]    entity
                Read-only reference to the Entity to add the Component to.

        @return   Reference to the Component of the type specified that was added.
        *//*****************************************************************************/
        template<typename Component>
        static Component& AddComponent(const Entity& entity);
        /****************************************************************************//*!
        @brief    Adds the specified Components to the specified Entity.

        @tparam       Components
                Argument pack of Components that should be added to the Entity.
        @param[in]    entity
                Read-only reference to the Entity to add the Components to.

        @return   EntityPack that contains the specified Entity and pointers to the added
                  Components.
        *//*****************************************************************************/
        template<typename ... Components>
        static EntityPack<Components ...> AddComponents(const Entity& entity);
        /****************************************************************************//*!
        @brief    Gets a Component to the specified Entity.
                
        @tparam       Component
                Type of Component to get from the specified Entity.
        @param[in]    entity
                Read-only reference to the Entity to get the Component from.

        @return   Pointer to the Component of the type specified. Nullptr if none exists.
        *//*****************************************************************************/
        template<typename Component>
        static Component* GetComponent(const Entity& entity);
        /****************************************************************************//*!
        @brief    Checks if the specified Entity has the specified Component.
        
                
        @tparam       Component
                Type of Component to check if the specified Entity contains.
        @param[in]    entity
                Read-only reference to the Entity to check if contains the Component.

        @return   True if the specified Entity has the specified Component.
        *//*****************************************************************************/
        template<typename Component>
        static bool HasComponent(const Entity& entity);
        /****************************************************************************//*!
        @brief    Removes a Component from the specified entity.

        @throws    std::invalid_argument
                If the Component does not exist to begin with.
                
        @tparam       Component
                Type of Component to remove from the specified Entity.
        @param[in]    entity
                Read-only reference to the Entity to remove a Component from.
        *//*****************************************************************************/
        template<typename Component>
        static void RemoveComponent(const Entity& entity);
        /****************************************************************************//*!
        @brief    Gets the number of Components of the specified type that are active.
                
        @tparam       Component
                Type of the Component to get a count of.

        @return   Number of Components of the specified type that are active.
        *//*****************************************************************************/
        template<typename Component>
        static ComponentStore::size_type GetComponentsCount();

        /****************************************************************************//*!
        @brief    Retrieves an unsorted range of IDs to Entities that has the specified
                  component.
                  
        @tparam       Component
                Type of the Component to get a list of Entities that have it.

        @return   A Range object that defines a list of Entity IDs that contain
        *//*****************************************************************************/
        template<typename Component>
        Range<typename ComponentStore::dense_const_iterator> GetEntities() const;
        
        /*-----------------------------------------------------------------------------*/
        /* Usage Functions - System                                                    */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Registers a SystemRoutine function into the ECS for execution in the
                  update of the ECS.

                  This also generates a cached SceneView based on the current Entities
                  and corresponding Components present.

                  As SceneViews can get fragmented over time when adding or removing
                  components, it is advised to call this after all objects have been
                  constructed and components added.
        @tparam       BaseSystem
                The type of BaseSystem that owns the SystemRoutine specified.
        @tparam       Components
                Argument pack of Components that this SystemRoutine targets.
        @param[in]    routine
                The routine to register into the system.
        *//*****************************************************************************/
        template<typename BaseSystem, typename ... Components>
        static void RegisterSystem(SystemRoutine<BaseSystem, Components...>* routine);

      private:
        static EntityManager                              entities;
        static ComponentStore                             components;
        static std::vector<SystemRoutineBase*>            systemsRoutines;
        static std::unordered_map<TypeId::InternalHashType, EntityPackListBase*> cachedEntityLists;
        static std::unordered_map<TypeId::InternalHashType, SceneViewBase*>      cachedSceneViews;
        // Event handler store
        static std::unordered_map<TypeId::InternalHashType, EntityEvent> onComponentAddedEvent;
        static std::unordered_map<TypeId::InternalHashType, EntityEvent> onComponentRemovedEvent;
        static EntityEvent                                               onEntityDestroyedEvent;

        /****************************************************************************//*!
        @brief    Helper function for updating of cached SceneViews when a new Component
                  has been added or removed.
                  
        @tparam       Component
                The type of Component to register changes for.
        @tparam       Components
                The type of Component combination of the cached SceneView that needs to be
                updated with changes.
        *//*****************************************************************************/
        template<typename Component, typename ... Components>
        static void registerComponentChangesEvents();
    };

    /*---------------------------------------------------------------------------------*/
    /* ECS Template Functions Definitions                                              */
    /*---------------------------------------------------------------------------------*/   
    template<typename ... Components>
    static EntityPack<Components ...> ECS::CreateEntity()
    {
        const Entity entity = ECS::CreateEntity();
        return AddComponents<Components ...>(entity);
    }
    template<typename Component>
    inline Component& ECS::AddComponent(const Entity& entity)
    {
        constexpr TypeId::InternalHashType COMPONENT_ID = TypeId::Get<Component>().GetId();
        Component& component = components.Add<Component>(entity);

        // Get the Event objeect to invoke or create if doesn't exist
        auto eventHandler = onComponentAddedEvent.find(COMPONENT_ID);
        if (eventHandler == onComponentAddedEvent.end())
        {
            eventHandler = onComponentAddedEvent.insert(std::make_pair(COMPONENT_ID, EntityEvent())).first;
        }

        // Execute the event
        eventHandler->second.Invoke(entity);
        return component;
    }
    template<typename ...Components>
    inline EntityPack<Components...> ECS::AddComponents(const Entity& entity)
    {
        return EntityPack<Components...>(entity, &AddComponent<Components>(entity) ...);
    }
    template<typename Component>
    inline Component* ECS::GetComponent(const Entity& entity)
    {
        return components.Get<Component>(entity);
    }
    template<typename Component>
    inline bool ECS::HasComponent(const Entity& entity)
    {
        return components.Has<Component>(entity);
    }
    template<typename Component>
    inline void ECS::RemoveComponent(const Entity& entity)
    {
        components.Remove<Component>(entity);
        onComponentRemovedEvent[TypeId::Get<Component>().GetId()].Invoke(entity);
    }
    template<typename Component>
    inline ComponentStore::size_type ECS::GetComponentsCount()
    {
        return components.GetCount<Component>();
    }
    template<typename Component>
    inline Range<typename ComponentStore::dense_const_iterator> ECS::GetEntities() const
    {
        return components.GetEntities<Component>();
    }

    template<typename BaseSystem, typename ... Components>
    void ECS::RegisterSystem(SystemRoutine<BaseSystem, Components...>* routine)
    {
        // Combination ID
        constexpr TypeId::InternalHashType COMBO_ID = TypeId::Get<Components ...>().GetId();

        // Create the required sceneview if not created before
        if (cachedEntityLists.find(COMBO_ID) == cachedEntityLists.end())
        {
            EntityPackList<Components...>* entityPackList = new EntityPackList<Components...>(components);
            SceneViewBase*      sceneView      = new SceneView<Components ...>(*entityPackList);
            cachedEntityLists.emplace(COMBO_ID, entityPackList);
            cachedSceneViews.emplace(COMBO_ID, sceneView);
            
            // Register an Add/Remove Component events to keep the SceneView updated when components are added
            (registerComponentChangesEvents<Components, Components ...>(), ...);
        }

        // Add the system routine
        systemsRoutines.push_back(routine);

    }

    template<typename Component, typename ... Components>
    inline void ECS::registerComponentChangesEvents()
    {
        constexpr TypeId::InternalHashType COMBO_ID = TypeId::Get<Components ...>().GetId();

        // Define all the anonymous functions needed
        auto onAdd = [COMBO_ID](const Entity& entity)
        {
            if ((ECS::HasComponent<Components>(entity) && ...))
            {
                // Add into the SceneView
                EntityPackList<Components ...>* sceneView = reinterpret_cast<EntityPackList<Components ...>*>(cachedEntityLists[COMBO_ID]);
                sceneView->insert(entity, ECS::GetComponent<Components>(entity) ...);
            }
        };
        auto onRemoved = [COMBO_ID](const Entity& entity)
        {
            // Remove from the SceneView
            EntityPackList<Components ...>* sceneView = reinterpret_cast<EntityPackList<Components ...>*>(cachedEntityLists[COMBO_ID]);
            sceneView->erase(entity);
        };

        // Hook up the events
        onComponentAddedEvent[TypeId::Get<Component>().GetId()]   += onAdd;
        onComponentRemovedEvent[TypeId::Get<Component>().GetId()] += onRemoved;
        onEntityDestroyedEvent                                    += onRemoved;
    }
}