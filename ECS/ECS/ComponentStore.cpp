#include "ComponentStore.h"


namespace Pls
{
    ComponentStore::~ComponentStore()
    {
        for (auto& set : sparseSetMaps)
        {
            delete set.second;
        }
        sparseSetMaps.clear();
    }

    void ComponentStore::RemoveAll(const Entity& entity)
    {
        for (auto& set : sparseSetMaps)
        {
            try
            {
                set.second->erase(entity.GetID());
            }
            catch (std::invalid_argument)
            {
                continue;
            }
        }
    }

    void ComponentStore::Clear()
    {
        for (auto& set : sparseSetMaps)
        {
            set.second->clear();
        }
    }
}